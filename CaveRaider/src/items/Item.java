package items;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.newdawn.slick.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import entities.TileEntity;

import Art.GameImages;
import world.World;

public class Item extends TileEntity implements Serializable {
	
	private static final long serialVersionUID = -397193795750873731L;
	
	public static DynamicElement[] items = new DynamicElement[256];
		
	private int imageX, imageY;
	
	private final long startTime;
		
	public Item(int id, int x, int y) {
		super(1, x, y);
		this.setItemID(id);
		startTime = System.currentTimeMillis();
		try {
			this.imageX = items[getItemID()].getAttributeAsIntegerList("image")[0];
			this.imageY = items[getItemID()].getAttributeAsIntegerList("image")[1];
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Item(int id) {
		super(1, 0, 0);
		this.setItemID(id);
		startTime = System.currentTimeMillis();
		try {
			this.imageX = items[getItemID()].getAttributeAsIntegerList("image")[0];
			this.imageY = items[getItemID()].getAttributeAsIntegerList("image")[1];
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Item(Map<String, Number> dataStore) {
		super(dataStore);
		startTime = System.currentTimeMillis();
		try {
			this.imageX = items[getItemID()].getAttributeAsIntegerList("image")[0];
			this.imageY = items[getItemID()].getAttributeAsIntegerList("image")[1];
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getItemID() {
		return this.getFromStore("itemid").intValue();
	}
	
	public void setItemID(int ID) {
		this.setValue("itemid", ID);
	}	
	
	public static void loadItems() {
		Document dom = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("xml/items.xml");
			dom = db.parse(is);
		} catch(Exception e) {
			e.printStackTrace();
		}
		Element tilesEle = dom.getDocumentElement();
		NodeList tiles = tilesEle.getElementsByTagName("item");
		if (tiles != null && tiles.getLength() > 0) {
			for (int i = 0; i < tiles.getLength(); i++) {
				Element item = (Element)tiles.item(i);
				int itemID = Integer.parseInt(item.getAttribute("id"));
				items[itemID] = new DynamicElement(item);
			}
		}
	}
	
	public static Image getImage(int id) {
		return GameImages.itemImages[items[id].getAttributeAsIntegerList("image")[0]][items[id].getAttributeAsIntegerList("image")[1]];
	}

	@Override
	public void update(GameContainer gc, World world, int delta) {

	}

	@Override
	public void render(Graphics g, int x, int y) {
		Image image = GameImages.itemImages[imageX][imageY];
		image.setRotation((float) ((Math.sin(((System.currentTimeMillis() - startTime) / (double)130) % (2*Math.PI))) * (180 / Math.PI)) / 6);
		g.drawImage(image, (x + BLOCK_SIZE * WORLD_SCALE / 2 - image.getWidth() / 2),  (y + BLOCK_SIZE * WORLD_SCALE / 2 - image.getHeight() / 2));
	}
	
}