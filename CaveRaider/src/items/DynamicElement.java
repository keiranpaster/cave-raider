package items;

import org.w3c.dom.Element;

public class DynamicElement {
	
	Element element;
		
	public DynamicElement(Element e) {
		this.element = e;
	}

	public String getAttribute(String a) {
		return element.getAttribute(a);
	}
	
	public int getAttributeAsInteger(String a) {
		try {
			return Integer.parseInt(element.getAttribute(a));
		} catch (Exception e) {
			return 0;
		}
	}
	
	public float getAttributeAsFloat(String a) {
		try {
			return Float.parseFloat(element.getAttribute(a));
		} catch (Exception e) {
			return 0;
		}
	}
	
	public boolean getAttributeAsBoolean(String a) {
		try {
			return Boolean.parseBoolean(element.getAttribute(a));
		} catch (Exception e) {
			return false;
		}
	}
	
	public int[] getAttributeAsIntegerList(String a) {
		try {
			String[] s = element.getAttribute(a).split(",");
			int[] r = new int[s.length];
			for (int i = 0; i < s.length; i++) {
				r[i] = Integer.parseInt(s[i]);
			}
			return r;
		} catch (Exception e) {
			e.printStackTrace();
			int[] r = {0,0};
			return r;
		}
	}
	
}
