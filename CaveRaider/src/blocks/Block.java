package blocks;

import java.io.InputStream;

import org.newdawn.slick.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import Art.GameImages;
import com.keirp.caveraider.Commons;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Block implements Commons {
	
	public static Block[] blocks = new Block[256];
		
	public Image image;
	
	private int health, itemID;
	private boolean lightSource;
	private boolean transparent;
		
	public Block(int id, int imageX, int imageY, int health, int itemID, boolean lightSource, boolean transparent) {
		this.lightSource = lightSource;
		this.transparent = transparent;
		this.health = health;
		this.itemID = itemID;
		this.image = GameImages.images[imageX][imageY];
	}
	
	public void render(Graphics g, int x, int y) {;
		g.drawImage(image, x, y);
	}
	
	public void update(GameContainer gc, int delta) {;
	
	}
	
	public boolean isLightSource() {
		return lightSource;
	}
	
	public boolean isTransparent() {
		return transparent;
	}
	
	public int getMaxHealth() {
		return health;
	}
	
	public int getItemID() {
		return itemID;
	}
	
	public void setLightSource(boolean lightSource) {
		this.lightSource = lightSource;
	}

	public void setTransparent(boolean transparent) {
		this.transparent = transparent;
	}
	
	public static void loadBlocks() {
		Document dom = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("xml/tiles.xml");
			dom = db.parse(is);
		} catch(Exception e) {
			e.printStackTrace();
		}
		Element tilesEle = dom.getDocumentElement();
		NodeList tiles = tilesEle.getElementsByTagName("tile");
		if (tiles != null && tiles.getLength() > 0) {
			for (int i = 0; i < tiles.getLength(); i++) {
				Element tile = (Element)tiles.item(i);
				int blockID = Integer.parseInt(tile.getAttribute("id"));
				String[] imageCoords = tile.getAttribute("image").split(",");
				int imageX = Integer.parseInt(imageCoords[0]);
				int imageY = Integer.parseInt(imageCoords[1]);
				int health = Integer.parseInt(tile.getAttribute("health"));
				int itemID = Integer.parseInt(tile.getAttribute("itemid"));
				boolean lightSource = Boolean.parseBoolean(tile.getAttribute("lightsource"));
				boolean transparent = Boolean.parseBoolean(tile.getAttribute("transparent"));
				blocks[blockID] = new Block(blockID, imageX, imageY, health, itemID, lightSource, transparent);
			}
		}
	}
	
}
