package inventory;

import items.Item;

import java.awt.Font;

import org.newdawn.slick.*;
import org.lwjgl.input.Mouse;

import player.Player;

import Art.GameImages;

import com.keirp.caveraider.Commons;

import entities.Entity;

import world.World;

public class Inventory implements Commons {

	private InventoryCompartment inventoryComp; 
	
	private boolean fullGridOpen;
	
	private Image hotBarGrid;
	private final String hotBarGridPath = "ui/hotBar.png";
	
	private Image selectedItemImage;
	private final String selectedItemImagePath = "ui/selectedItem.png";
	
	private Image fullGridSelectionImage;
	private final String fullGridSelectionImagePath = "ui/fullGridSelection.png";
	
	private Image fullGrid;
	private final String fullGridPath = "ui/fullInventory.png";
	
	private int selectedItemIndex;
	
	private long lastPlace;
	
	Font font;
	TrueTypeFont ttf;
		
	InventoryItem mouseItem = new InventoryItem(0, 0, 0);
	
	private InventoryCoordinate selectedCoordinate = new InventoryCoordinate();
		
	public Inventory() {
		font = new Font("Verdana", Font.BOLD, 15);
		ttf = new TrueTypeFont(font, true);
		resetPlacedTime();
		inventoryComp = new InventoryCompartment(INVENTORY_ROWS, INVENTORY_COLUMNS);
		
		fullGridOpen = false;
		try {
			hotBarGrid = new Image(hotBarGridPath);
		} catch (SlickException e) {
			// TODO Auto-generated catch block
		}
		
		try {
			fullGrid = new Image(fullGridPath);
		} catch (SlickException e) {
			// TODO Auto-generated catch block
		}
		
		try {
			selectedItemImage = new Image(selectedItemImagePath);
		} catch (SlickException e) {
			// TODO Auto-generated catch block
		}
		
		try {
			fullGridSelectionImage = new Image(fullGridSelectionImagePath);
		} catch (SlickException e) {
			// TODO Auto-generated catch block
		}
		
		selectedItemIndex = 0;
	}
		
	public boolean pushItem(InventoryItem item) {
		InventoryCoordinate coordinate = getSlot(item);
		if (coordinate.slotExists()) {
			addItem(item, coordinate.getX(), coordinate.getY());
			return true;
		} else {
			return false;
		}
	}
	
	public void addItem(InventoryItem item, int x, int y) {
		if (inventoryComp.getItem(x, y).getItemID() == 0) {
			item.setPosition(x, y);
			inventoryComp.addItem(item, x, y);
		} else {
			int extra = inventoryComp.getItem(x, y).addToStack(item.getStack());
			if (extra > 0) {
				InventoryItem extraStack;
				try {
					extraStack = item.getClass().newInstance();
					extraStack.setStack(extra);
					pushItem(extraStack);
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
	}
		
	public boolean isFullGridOpen() {
		return fullGridOpen;
	}
	
	public void setFullGridOpen(boolean open) {
		fullGridOpen = open;
	}
	
	public Image getHotBarImage() {
		return hotBarGrid;
	}
	
	public Image getSelectedItemImage() {
		return selectedItemImage;
	}
	
	public Image getFullGridImage() {
		return fullGrid;
	}
	
	public Image getFullGridSelectionImage() {
		return fullGridSelectionImage;
	}
	
	public int getHotBarX() {
		return (SCREEN_WIDTH - INVENTORY_WIDTH) / 2;
	}
	
	public int getHotBarY() {
		return SCREEN_HEIGHT - (INVENTORY_ITEM_SIZE + BOTTOM_INVENTORY_SPACING + SIDE_INVENTORY_SPACING);
	}
	
	public int getFullGridX() {
		return (SCREEN_WIDTH - INVENTORY_WIDTH) / 2;
	}
	
	public int getFullGridY() {
		return SCREEN_HEIGHT - (INVENTORY_ITEM_SIZE + BOTTOM_INVENTORY_SPACING + SIDE_INVENTORY_SPACING) - ((INVENTORY_ITEM_SIZE + INVENTORY_SPACING) * (INVENTORY_ROWS - 1)) - INVENTORY_HOTBAR_SPACING;
	}
	
	public int getSelectedItemX() {
		int x = getXForInventoryCoordinateX(selectedItemIndex);
		return x - SELECTED_ITEM_DIFF;
	}
	
	public int getSelectedItemY() {
		return SCREEN_HEIGHT - INVENTORY_ITEM_SIZE - BOTTOM_INVENTORY_SPACING - SELECTED_ITEM_DIFF;
	}
	
	public int getFullGridSelectionX() {
		return getXForInventoryCoordinateX(selectedCoordinate.getX());
	}
	
	public int getFullGridSelectionY() {
		return getYForInventoryCoordinateY(selectedCoordinate.getY());
	}
	
	public void setSelectedItem(int itemIndex) {
		selectedItemIndex = itemIndex;
	}
	
	public InventoryItem getMouseItem() {
		return mouseItem;
	}
	
	public boolean shouldRenderMouseItem() {
		return (mouseItem.getItemID() != 0 && fullGridOpen);
	}
	
	public void nextSelectedItem() {
		selectedItemIndex++;
		if (selectedItemIndex > INVENTORY_COLUMNS - 1) {
			selectedItemIndex = 0;
		}
	}
	
	public void previousSelectedItem() {
		selectedItemIndex--;
		if (selectedItemIndex < 0) {
			selectedItemIndex = INVENTORY_COLUMNS - 1;
		}
	}
	
	public boolean isSelectedItemFilled() {
		if (inventoryComp.getItem(selectedItemIndex, 0).getItemID() == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public InventoryItem getSelectedItem() {
		return inventoryComp.getItem(selectedItemIndex, 0);
	}
	
	public boolean shouldShowItem() {
		return !fullGridOpen;
	}
	
	public int getDeltaFromLastPlace() {
		return (int) (System.currentTimeMillis() - lastPlace);
	}
	
	public void resetPlacedTime() {
		lastPlace = System.currentTimeMillis();
	}
	
	public void render(Graphics g) {
		g.drawImage(getHotBarImage(), getHotBarX(), getHotBarY());
		
		if (isFullGridOpen()) {
			g.drawImage(getFullGridImage(), getFullGridX(), getFullGridY());
			
		} else {
			g.drawImage(getSelectedItemImage(), getSelectedItemX(), getSelectedItemY());
		}
				
		for (int i = 0; i < inventoryComp.getColumns(); i++) {
			for (int c = 0; c < inventoryComp.getRows(); c++) {
					if (inventoryComp.getItem(i, c).getItemID() != 0) {
							if (isFullGridOpen() || inventoryComp.getItem(i, c).getY() == 0) {
								g.drawImage(GameImages.inventoryImages[inventoryComp.getItem(i, c).getImageX()][inventoryComp.getItem(i, c).getImageY()], this.getXForInventoryCoordinateX(inventoryComp.getItem(i, c).getX()), this.getYForInventoryCoordinateY(inventoryComp.getItem(i, c).getY()));
								if (inventoryComp.getItem(i, c).getStack() != 1) {
									ttf.drawString(this.getXForInventoryCoordinateX(inventoryComp.getItem(i, c).getX()) + INVENTORY_ITEM_SIZE - STACK_NUMBER_INSET, this.getYForInventoryCoordinateY(inventoryComp.getItem(i, c).getY()) + INVENTORY_ITEM_SIZE - STACK_NUMBER_INSET - 7, String.format("%d", inventoryComp.getItem(i, c).getStack()));
								}
							}
					}
			}
		}
		
		if (isFullGridOpen()) {
			g.drawImage(getFullGridSelectionImage(), getFullGridSelectionX(), getFullGridSelectionY());
		}
		
		if (shouldRenderMouseItem()) {
			g.drawImage(GameImages.inventoryImages[getMouseItem().getImageX()][getMouseItem().getImageY()], getMouseItem().getX(), getMouseItem().getY());
			if (getMouseItem().getStack() != 1) {
				ttf.drawString(getMouseItem().getStackX(), getMouseItem().getStackY(), String.format("%d", getMouseItem().getStack()));
			}
		}
				
//		if (shouldShowItem() && isSelectedItemFilled()) {
//			g.drawImage(GameImages.inventoryImages[getSelectedItem().getImageX()][getSelectedItem().getImageY()], input.getMouseX(), input.getMouseY());
//		}
	}
	
	public void update(GameContainer gc, World world, Player player) {
		Input input = gc.getInput();
		
		if (!fullGridOpen) {
			if (input.isKeyDown(Input.KEY_1)) {
				selectedItemIndex = 0;
			}
			if (input.isKeyDown(Input.KEY_2)) {
				selectedItemIndex = 1;
			}
			if (input.isKeyDown(Input.KEY_3)) {
				selectedItemIndex = 2;
			}
			if (input.isKeyDown(Input.KEY_4)) {
				selectedItemIndex = 3;
			}
			if (input.isKeyDown(Input.KEY_5)) {
				selectedItemIndex = 4;
			}
			if (input.isKeyDown(Input.KEY_6)) {
				selectedItemIndex = 5;
			}
			if (input.isKeyDown(Input.KEY_7)) {
				selectedItemIndex = 6;
			}
			if (input.isKeyDown(Input.KEY_8)) {
				selectedItemIndex = 7;
			}
			if (input.isKeyDown(Input.KEY_9)) {
				selectedItemIndex = 8;
			}
			if (input.isKeyPressed(Input.KEY_R)) {
				InventoryItem item = getSelectedItem();
				if (item.getItemID() != 0) {
					item.dropItem(world, player.getBlockX(), player.getBlockY());
					if (item.getStack() < 1) {
						InventoryCoordinate coordinate = new InventoryCoordinate(item.getX(), item.getY());
						clearInventorySlot(coordinate.getX(), coordinate.getY());
					}
				}
			}
			int change = Mouse.getDWheel();
			if (change > 0) {
				nextSelectedItem();
			} else if (change < 0) {
				previousSelectedItem();
			}
			
		} else {
			if (input.isMousePressed(0)) {
				if (isMouseInInventory(input)) {
					InventoryItem item = getMouseItem();
					if (item.getItemID() == inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).getItemID()) {
						int extra = inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).addToStack(item.getStack());
						if (extra == 0) {
							clearMouseItem();
						} else {
							mouseItem.setStack(extra);
						}
					} else {
						setMouseItem(inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()));
						inventoryComp.addItem(item, selectedCoordinate.getX(), selectedCoordinate.getY());
						item.setPosition(getXForInventoryCoordinateX(selectedCoordinate.getX()), getYForInventoryCoordinateY(selectedCoordinate.getY()));
						item.setPosition(selectedCoordinate.getX(), selectedCoordinate.getY());
					}
				}
			}
			
			if (input.isMousePressed(1)) {
				if (isMouseInInventory(input)) {
					InventoryItem item = getMouseItem();
					if (item.getItemID() == 0) {
						if (inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).getItemID() != 0) {
							int halfOfStack = (int) Math.ceil((double) inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).getStack() / (double) 2);
							int originalItemStack = inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).getStack() - halfOfStack;
							setMouseItem(inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()));
							clearInventorySlot(selectedCoordinate.getX(), selectedCoordinate.getY());
							mouseItem.setStack(halfOfStack);
							if (originalItemStack > 0) {
								inventoryComp.addItem(new InventoryItem(mouseItem.getItemID()), selectedCoordinate.getX(), selectedCoordinate.getY());
								inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).setStack(originalItemStack);
								inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).setPosition(getXForInventoryCoordinateX(selectedCoordinate.getX()), getYForInventoryCoordinateY(selectedCoordinate.getY()));
								inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).setPosition(selectedCoordinate.getX(), selectedCoordinate.getY());

							}
						}
					} else {
						if ((item.getItemID() == inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).getItemID() && inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).getStack() < inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).getMaxStackSize()) || inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).getItemID() == 0) {
							if (item.getItemID() == inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).getItemID()) {
								mouseItem.addToStack(-1);
								if (mouseItem.getStack() <= 0) {
									clearMouseItem();
								}
								inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).addToStack(1);
							} else {
								mouseItem.addToStack(-1);
								inventoryComp.addItem(new InventoryItem(mouseItem.getItemID()), selectedCoordinate.getX(), selectedCoordinate.getY());
								inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).setStack(1);
								inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).setPosition(getXForInventoryCoordinateX(selectedCoordinate.getX()), getYForInventoryCoordinateY(selectedCoordinate.getY()));
								inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()).setPosition(selectedCoordinate.getX(), selectedCoordinate.getY());	
								if (mouseItem.getStack() <= 0) {
									clearMouseItem();
								}
							}
						} else {
							setMouseItem(inventoryComp.getItem(selectedCoordinate.getX(), selectedCoordinate.getY()));
							inventoryComp.addItem(item, selectedCoordinate.getX(), selectedCoordinate.getY());
							item.setPosition(getXForInventoryCoordinateX(selectedCoordinate.getX()), getYForInventoryCoordinateY(selectedCoordinate.getY()));
							item.setPosition(selectedCoordinate.getX(), selectedCoordinate.getY());
						}
					}
				}
			}
			if (isMouseInInventory(input)) {
				selectedCoordinate = getInventoryCoordinateFromCoordinate(input.getMouseX(), input.getMouseY());
			}
			mouseItem.setPosition(input.getMouseX(), input.getMouseY());
		}
				
		if (input.isKeyPressed(Input.KEY_E)) {
			setFullGridOpen(!isFullGridOpen());
		}
		
	}
	
	public void useItem(InventoryItem item, int x, int y, Player p, World world) {
		if (item.useItem(this, x, y, p, world)) {
			item.addToStack(-1);
			if (item.getStack() < 1) {
				InventoryCoordinate coordinate = new InventoryCoordinate(item.getX(), item.getY());
				clearInventorySlot(coordinate.getX(), coordinate.getY());
			}
		}
	}
	
	private void setMouseItem(InventoryItem item) {
		this.mouseItem = item;
	}
	
	private boolean isMouseInInventory(Input input) {
		if (fullGridOpen) {
			if (input.getMouseY() > getFullGridY() && input.getMouseX() > getHotBarX() && input.getMouseX() < INVENTORY_WIDTH + getHotBarX()) {
				return true;
			} else {
				return false;
			}
		} else {
			if (input.getMouseY() > getHotBarY() && input.getMouseX() > getHotBarX() && input.getMouseX() < INVENTORY_WIDTH + getHotBarX()) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public int getFirstArrowID() throws Exception {
		for (int i = 0; i < inventoryComp.getColumns(); i++) {
			for (int c = 0; c < inventoryComp.getRows(); c++) {
				if (Item.items[inventoryComp.getItem(i, c).getItemID()].getAttribute("user").equals("bow")) {
					inventoryComp.getItem(i, c).addToStack(-1);
					int id = inventoryComp.getItem(i, c).getItemID();
					if (inventoryComp.getItem(i, c).getStack() < 1) {
						InventoryCoordinate coordinate = new InventoryCoordinate(i, c);
						clearInventorySlot(coordinate.getX(), coordinate.getY());
					}
					return id;				
				}
			}
		}
		throw new Exception();
	}
	
	private InventoryCoordinate getSlot(InventoryItem item) {
		boolean slotAvailable = false;
		boolean shouldStack = false;
		InventoryCoordinate coordinate = null;
		
		for (int c = 0; c < INVENTORY_ROWS; c++) {
			for (int i = 0; i < INVENTORY_COLUMNS; i++) {
				if (inventoryComp.getItem(i, c).getItemID() == item.getItemID() && inventoryComp.getItem(i, c).getStack() < inventoryComp.getItem(i, c).getMaxStackSize()) {
					shouldStack = true;
					slotAvailable = true;
					coordinate = new InventoryCoordinate(i, c);
					break;
				}
			}
			if (slotAvailable) {
				break;
			}
		}
		
		if (!shouldStack) {
			for (int c = 0; c < INVENTORY_ROWS; c++) {
				for (int i = 0; i < INVENTORY_COLUMNS; i++) {
					if (inventoryComp.getItem(i, c).getItemID() == 0) {
						slotAvailable = true;
						coordinate = new InventoryCoordinate(i, c);
						break;
					}
				}
				if (slotAvailable) {
					break;
				}
			}
		}
		
		if (slotAvailable) {
			return coordinate;
		} else {
			return new InventoryCoordinate();
		}
	}
	
	private int getXForInventoryCoordinateX(int x) {
		int firstInventoryItemX = (SCREEN_WIDTH - INVENTORY_WIDTH) / 2 + SIDE_INVENTORY_SPACING;
		return firstInventoryItemX + (INVENTORY_ITEM_SIZE + INVENTORY_SPACING) * (x);
	}
	
	private int getYForInventoryCoordinateY(int y) {
		int firstInventoryItemY = SCREEN_HEIGHT - INVENTORY_ITEM_SIZE - BOTTOM_INVENTORY_SPACING;
		int spacer = 0;
		if (y > 0) {
			spacer = INVENTORY_HOTBAR_SPACING;
		}
		return firstInventoryItemY - (INVENTORY_ITEM_SIZE + INVENTORY_SPACING) * (y) - spacer;
	}
	
	private InventoryCoordinate getInventoryCoordinateFromCoordinate(int x, int y) {
		if (y < SCREEN_HEIGHT - INVENTORY_ITEM_SIZE - BOTTOM_INVENTORY_SPACING) {
			y += INVENTORY_HOTBAR_SPACING;
		}
		int invY = (SCREEN_HEIGHT - y - 5) / (INVENTORY_ITEM_SIZE + INVENTORY_SPACING);
		int invX = (x - (SCREEN_WIDTH - INVENTORY_WIDTH) / 2 + SIDE_INVENTORY_SPACING  - (INVENTORY_ITEM_SIZE / 2)) / (INVENTORY_ITEM_SIZE + INVENTORY_SPACING);
		return new InventoryCoordinate(invX, invY);
	}
	
	private void clearInventorySlot(int x, int y) {
		inventoryComp.clearSlot(x, y);
	}
	
	private void clearMouseItem() {
		mouseItem = null;
		mouseItem = new InventoryItem(0, 0, 0);
	}
	
	private class InventoryCoordinate {
		
		private final int x;
		private final int y;
		private final boolean exists;
		
		public InventoryCoordinate(int x, int y) {
			this.x = x;
			this.y = y;
			this.exists = true;
		}
		
		public InventoryCoordinate() {
			this.exists = false;
			this.x = 0;
			this.y = 0;
		}
		
		public boolean slotExists() {
			return exists;
		}
		
		public int getX() {
			return x;
		}
		
		public int getY() {
			return y;
		}
		
		@Override
		public String toString() {
			return "X: " + x + " Y: " + y;
		}
		
	}
	
}
