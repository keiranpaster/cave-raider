package inventory;

public abstract class Compartment {

	private InventoryItem[][] itemGrid;
	private int x;
	private int y;
	private int rows;
	private int columns;
		
	public Compartment(int rows, int columns) {
		this.x = 0;
		this.y = 0;
		this.rows = rows;
		this.columns = columns;
		itemGrid = new InventoryItem[columns][rows];
		for (int i = 0; i < columns; i++) {		
			for (int c = 0; c < rows; c++) {
				itemGrid[i][c] = new InventoryItem(0);
			}
		}
	}
	
	public boolean addItem(InventoryItem i, int px, int py) {
		if (acceptItem(i, px, py)) {
			itemGrid[px][py] = i;
			return true;
		}
		return false;
	}
	
	public int getRows() {
		return rows;
	}
	
	public int getColumns() {
		return columns;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public InventoryItem getItem(int px, int py) {
		return itemGrid[px][py];
	}
	
	public void clearSlot(int px, int py) {
		itemGrid[px][py] = null;
		itemGrid[px][py] = new InventoryItem(0);
	}
	
	public abstract boolean acceptItem(InventoryItem i, int px, int py);
				
}
