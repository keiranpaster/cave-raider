package inventory;

import player.Player;
import items.DynamicElement;
import items.Item;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Game;

import entities.Arrow;
import entities.Entity;
import entities.ItemEffect;
import entities.Mob;
import entities.Projectile;

import world.World;

public class InventoryItem implements Commons {
		
	private boolean isBlock;
	private int itemID;
	private int blockID;
	
	private int x;
	private int y;
		
	private int stack;
		
	private int maxStackSize = MAX_STACK_SIZE;
	
	private int minRangeToUse;
	
	private String type;
	
	private int imageX, imageY;
	
	private DynamicElement element;
	
	public InventoryItem(int id, int x, int y) {
		this.itemID = id;
		this.x = x;
		this.y = y;
		this.stack = 1;
		this.imageX = Item.items[id].getAttributeAsIntegerList("image")[0];
		this.imageY = Item.items[id].getAttributeAsIntegerList("image")[1];
		this.type = Item.items[id].getAttribute("type");
		this.element = Item.items[id];
		if (this.type.equals("block")) {
			this.minRangeToUse = 1;
			this.blockID = Item.items[id].getAttributeAsInteger("blockid");
		} else {
			if (this.element.getAttributeAsInteger("range") != 0) {
				this.minRangeToUse = Item.items[id].getAttributeAsInteger("range");
			} else {
				this.minRangeToUse = 1;
			}
		}
	}
	
	public InventoryItem(int id) {
		this.itemID = id;
		this.x = 0;
		this.y = 0;
		this.stack = 1;
		this.imageX = Item.items[id].getAttributeAsIntegerList("image")[0];
		this.imageY = Item.items[id].getAttributeAsIntegerList("image")[1];
		this.type = Item.items[id].getAttribute("type");
		this.element = Item.items[id];
		if (this.type.equals("block")) {
			this.minRangeToUse = 1;
			this.blockID = Item.items[id].getAttributeAsInteger("blockid");
		} else {
			if (this.element.getAttributeAsInteger("range") != 0) {
				this.minRangeToUse = Item.items[id].getAttributeAsInteger("range");
			} else {
				this.minRangeToUse = 1;
			}
		}
	}
	
	public void setItemID(int item) {
		this.itemID = item;
	}
	
	public void setBlockID(int id) {
		this.blockID = id;
		this.isBlock = true;
	}
	
	public int getX() {
		return (int) x;
	}
	
	public int getY() {
		return (int) y;
	}
	
	public int getStackX() {
		return (int) x + INVENTORY_ITEM_SIZE - STACK_NUMBER_INSET;
	}
	
	public int getStackY() {
		return (int) y + INVENTORY_ITEM_SIZE - STACK_NUMBER_INSET - 7;
	}
	
	public int getMaxStackSize() {
		return maxStackSize;
	}

	public void setMaxStackSize(int maxStackSize) {
		this.maxStackSize = maxStackSize;
	}

	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
		
	public int getImageX() {
		return imageX;
	}

	public int getImageY() {
		return imageY;
	}
	
	public void setStack(int stack) {
		this.stack = stack;
	}
	
	public int addToStack(int add) {
		if (stack + add > getMaxStackSize()) {
			int returnValue = stack + add - getMaxStackSize();
			this.stack = getMaxStackSize();
			return returnValue;
		} else {
			this.stack += add;
			return 0;
		}
	}
	
	public int getStack() {
		return stack;
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
		
	public boolean isBlock() {
		return isBlock;
	}
	
	public int getItemID() {
		return itemID;
	}
	
	public int getMinRange() {
		return minRangeToUse;
	}
	
	public boolean useItem(Inventory i, int x, int y, Player p, World world) {
		if (this.element.getAttribute("target").equals("mob")) {
			Game.debug("dist: " + world.distanceBetween(p, p.getTargetMob()));
			if (world.distanceBetween(p, p.getTargetMob()) > this.minRangeToUse || !world.lineOfSight(p.getBlockX(), p.getBlockY(), p.getTargetMob().getBlockX(), p.getTargetMob().getBlockY())) {
				Game.debug("min range " + this.minRangeToUse);
				return false;
			}
		}
		if (type.equals("block")) {
			if (i.getDeltaFromLastPlace() >= MIN_TIME_BETWEEN_PLACE) {
				if (world.addBlock(blockID, x, y)) {
					i.resetPlacedTime();
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else if (type.equals("pickaxe")) {
			if (world.damageBlock(this.element.getAttributeAsInteger("blockdamage"), x, y)) {
				return false;
			}
			return false;
		} else if (type.equals("bow")) {
			Game.debug("min range to use: " + this.minRangeToUse);
			Game.debug(p.getDirection());
			try {
				int arrowID = i.getFirstArrowID();
				float speed = Item.items[arrowID].getAttributeAsFloat("speed");
				final int damage = Item.items[arrowID].getAttributeAsInteger("damage");
				world.addEntity(new Projectile(arrowID, p.getX(), p.getY(), p.getTargetMob(), speed));
			} catch (Exception e) {
				
			}
			return false;
		} else if (type.equals("sword")) {
				ItemEffect ie = new ItemEffect(this.element.getAttribute("effect"), p.getTargetMob(), null);
				ie.done();
				return false;
		} else {
			return false;
		}
	}
	
	public boolean dropItem(World world, int x, int y) {
		boolean dropped = world.addTileEntity(new Item(itemID), x, y);
		if (dropped) {
			this.addToStack(-1);
		}
		return dropped;
	}

}
