package entities;

import java.io.Serializable;
import java.util.Map;

import com.keirp.caveraider.Commons;

public abstract class TileEntity extends Entity implements Commons, Serializable {
	
	private static final long serialVersionUID = -3972126593885483417L;
	int blockX, blockY;

	public TileEntity(int id, int x, int y) {
		super(id, x, y);
	}
	
	public TileEntity(Map<String, Number> dataStore) {
		super(dataStore);
	}
	
	public void setBlockX(int blockX) {
		this.setValue("blockx", blockX);
		this.blockX = blockX;
		this.setX(blockX * BLOCK_SIZE);
	}
	
	public void setBlockY(int blockY) {
		this.setValue("blocky", blockY);
		this.setY(blockY * BLOCK_SIZE);
	}
	
	@Override
	public int getBlockX() {
		return this.getFromStore("blockx").intValue();
	}
	
	@Override
	public int getBlockY() {
		return this.getFromStore("blocky").intValue();
	}
	
	public void setBlockPosition(int blockX, int blockY) {
		this.setValue("blockx", blockX);
		this.setX(blockX * BLOCK_SIZE);
		this.setValue("blocky", blockY);
		this.setY(blockY * BLOCK_SIZE);
	}

}
