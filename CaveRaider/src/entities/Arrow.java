package entities;

import items.Item;

import java.util.Map;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

import world.World;

import Art.GameImages;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Play;

public class Arrow extends Entity implements Commons {
	
	public Arrow(int id, int x, int y, double angle) {
		super(2, x, y);
		this.setBounds(new Rectangle(0, 0, 2, 2));
		this.setOffset(7, 7);
		this.setCenter(16, 8);
		this.setValue("arrowid", id);
		this.setRotation(angle);
		this.setValue("dx", Math.cos(angle*(Math.PI/180)));
		this.setValue("dy", Math.sin(angle*(Math.PI/180)));

	}
	
	public Arrow(Map<String, Number> dataStore) {
		super(dataStore);
		this.setBounds(new Rectangle(0, 0, 16, 16));
		this.setCenter(16, 8);
		this.setOffset(7, 0);
		this.setRotation(this.getRotation());
	}

	@Override
	public void update(GameContainer gc, World world, int delta) {
		this.changeX(this.getFromStore("dx"));
		this.changeY(this.getFromStore("dy"));
//		this.setRotation(this.getRotation() + 1);
		if (world.isTouchingBlock(this)) {
			this.setValue("dx", 0);
			this.setValue("dy", 0);
		}
	}

	@Override
	public void render(Graphics g, int x, int y) {
		Image i = GameImages.itemImages[Item.items[this.getFromStore("arrowid").intValue()].getAttributeAsIntegerList("image")[0]][Item.items[this.getFromStore("arrowid").intValue()].getAttributeAsIntegerList("image")[1]];
		i.setCenterOfRotation(16 * WORLD_SCALE, 9 * WORLD_SCALE);
		i.setRotation(this.getRotation());
		g.drawImage(i, x, y);
//		Play.ttf.drawString(x, y, ".");
//		Play.ttf.drawString(this.getBounds().getMinX() * WORLD_SCALE - World.cameraX, this.getBounds().getMinY() * WORLD_SCALE - World.cameraY, ",");
//		Play.ttf.drawString(this.getBounds().getMinX() * WORLD_SCALE - World.cameraX, this.getBounds().getMaxY() * WORLD_SCALE - World.cameraY, ",");
//		Play.ttf.drawString(this.getBounds().getMaxX() * WORLD_SCALE - World.cameraX, this.getBounds().getMaxY() * WORLD_SCALE - World.cameraY, ",");
//		Play.ttf.drawString(this.getBounds().getMaxX() * WORLD_SCALE - World.cameraX, this.getBounds().getMinY() * WORLD_SCALE - World.cameraY, ",");

	}
	
}