package entities;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.keirp.caveraider.Game;

import world.MovementIntent;
import world.ShiftedPath;
import world.World;

public abstract class Mob extends Entity {
	
	private int attackTime;
	private int detectionRadius;
	private float movementSpeed;
	MovementIntent currentMovementIntent;
	private ShiftedPath currentPath;
	private int updatePeriod;
	private long lastUpdate = 0;
	private Queue<Entity> toSpawn = new LinkedList<Entity>();
	
	private Queue<int[]> animateQueue = new LinkedList<int[]>();

	public Mob(int id, int x, int y) {
		super(id, x, y);
		setDirection(0);
	}
	
	public Mob(Map<String, Number> dataStore) {
		super(dataStore);
	}

	@Override
	public abstract void update(GameContainer gc, World world, int delta);

	@Override
	public abstract void render(Graphics g, int x, int y);

	public float getHealth() {
		return this.getFromStore("health").floatValue();
	}

	public void setHealth(float health) {
		this.setValue("health", health);
	}
	
	public void changeHealth(float dh) {
		toSpawn.add(new TextParticle(this.getX(), this.getY() - 10, String.valueOf(dh)));
		this.setValue("health", getHealth() + dh);
	}
	
	public boolean isDead() {
		if (getHealth() <= 0) {
			return true;
		}
		return false;
	}

	public void spawnEntities(World w) {
		while (!toSpawn.isEmpty()) {
			w.addEntity(toSpawn.poll());
		}
	}
	
	public int getAttackTime() {
		return attackTime;
	}
	
	public void setUpdatePeriod(int p) {
		updatePeriod = p;
	}

	public void setAttackTime(int attackTime) {
		this.attackTime = attackTime;
	}

	public int getDetectionRadius() {
		return detectionRadius;
	}

	public void setDetectionRadius(int detectionRadius) {
		this.detectionRadius = detectionRadius;
	}
	
	public void setMovementSpeed(float movementSpeed) {
		this.movementSpeed = movementSpeed * (WORLD_SCALE / 2);
	}
	
	public void animateMovements(int delta, World w) {
		try {
			if (!animateQueue.isEmpty()) {
				if (Math.abs(animateQueue.peek()[0] * BLOCK_SIZE - this.getX()) < movementSpeed * delta && Math.abs(animateQueue.peek()[1] * BLOCK_SIZE - this.getY()) < movementSpeed * delta) {
					double diffX = movementSpeed - Math.abs(this.getX() - animateQueue.peek()[0] * BLOCK_SIZE);
					double diffY = movementSpeed - Math.abs(this.getY() - animateQueue.peek()[1] * BLOCK_SIZE);
					double diff;
					if (diffX > diffY) {
						diff = diffX;
					} else {
						diff = diffY;
					}
					this.setX(animateQueue.peek()[0] * BLOCK_SIZE);
					this.setY(animateQueue.peek()[1] * BLOCK_SIZE);
					w.removePathing(animateQueue.peek()[0], animateQueue.peek()[1]);
					animateQueue.remove();
					if (!animateQueue.isEmpty()) {
						if (animateQueue.peek()[0] * BLOCK_SIZE > this.getX()) {
							this.changeX(diff * delta);
							this.setDirection(3);
						} else if (animateQueue.peek()[0] * BLOCK_SIZE < this.getX()) {
							this.changeX(-diff * delta);
							this.setDirection(2);
						} else if (animateQueue.peek()[1] * BLOCK_SIZE > this.getY()) {
							this.changeY(diff * delta);
							this.setDirection(0);
						} else if (animateQueue.peek()[1] * BLOCK_SIZE < this.getY()) {
							this.changeY(-diff * delta);
							this.setDirection(1);
						}
					} else {
						if (currentMovementIntent != null) {
							currentMovementIntent.movementFinished();
							currentMovementIntent = null;
						}
					}
				} else {
					if (Math.abs(animateQueue.peek()[0] * BLOCK_SIZE - this.getX()) < movementSpeed) {
						this.setX(animateQueue.peek()[0] * BLOCK_SIZE);
					}
					if (Math.abs(animateQueue.peek()[1] * BLOCK_SIZE - this.getY()) < movementSpeed) {
						this.setY(animateQueue.peek()[1] * BLOCK_SIZE);
					}
					if (animateQueue.peek()[0] * BLOCK_SIZE > this.getX()) {
						this.changeX(movementSpeed * delta);
						this.setDirection(3);
					} else if (animateQueue.peek()[0] * BLOCK_SIZE < this.getX()) {
						this.changeX(-movementSpeed * delta);
						this.setDirection(2);
					} else if (animateQueue.peek()[1] * BLOCK_SIZE > this.getY()) {
						this.changeY(movementSpeed * delta);
						this.setDirection(0);
					} else if (animateQueue.peek()[1] * BLOCK_SIZE < this.getY()) {
						this.changeY(-movementSpeed * delta);
						this.setDirection(1);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int getDirection() {
		return this.getFromStore("direction").intValue();
	}
	
	public void setDirection(int direction) {
		this.setValue("direction", direction);
	}
	
	public void pushToAnimateQueue(int[] animation) {
		animateQueue.add(animation);
	}
	
	public int[] peekAnimateQueue() {
		return animateQueue.peek();
	}
	
	public void clearAnimateQueue() {
		animateQueue.clear();
	}
	
	public boolean hasAnimate() {
		return !animateQueue.isEmpty();
	}
	
	public boolean canUpdate() {
		return lastUpdate + updatePeriod < System.currentTimeMillis();
	}
	
	public void updatePath() {
		lastUpdate = System.currentTimeMillis();
	}
	
	public void pathToQueue(ShiftedPath path, World world) {
		if (currentPath == null || currentPath != path) {
			removePath(world);
			currentPath = path;
			this.currentMovementIntent = null;
			int[] nextMove = animateQueue.peek();
			if (!animateQueue.isEmpty() && path.contains(nextMove)) {
				try {
					path.removePathUpTo(path.getPositionOf(nextMove));
				} catch (Exception e) {
					
				}
			}
			this.clearAnimateQueue();
			if (nextMove != null) {
				animateQueue.add(nextMove);
			}
			for(int i = 0; i < path.getLength(); i++) {
				int[] animationStep = {
						path.getX(i),
						path.getY(i)
				};
				this.pushToAnimateQueue(animationStep);
				world.addPathing(this, animationStep[0], animationStep[1]);
	        }
		}
	}
	
	public void pathToQueueWithIntent(ShiftedPath path, MovementIntent mi, World world) {
		if (currentPath == null || currentPath != path) {
			removePath(world);
			currentPath = path;
			this.currentMovementIntent = mi;
			boolean shouldMove = true;
			if (currentMovementIntent.shouldStopMoving((int)Math.floor(this.getX() / BLOCK_SIZE), (int)Math.floor(this.getY() / BLOCK_SIZE), world)) {
				currentMovementIntent.movementFinished();
				currentMovementIntent = null;
				shouldMove = false;
			}
			if (shouldMove) {
				int[] nextMove = animateQueue.peek();
				if (!animateQueue.isEmpty() && path.contains(nextMove)) {
					try {
						path.removePathUpTo(path.getPositionOf(nextMove));
					} catch (Exception e) {
						
					}
				}
				this.clearAnimateQueue();
				if (nextMove != null) {
					animateQueue.add(nextMove);
				}
				for(int i = 0; i < path.getLength(); i++) {
					int[] animationStep = {
							path.getX(i),
							path.getY(i)
					};
					if (currentMovementIntent.shouldStopMoving(animationStep[0], animationStep[1], world)) {
						break;
					}
					this.pushToAnimateQueue(animationStep);
					world.addPathing(this, animationStep[0], animationStep[1]);
		        }
			}
		}
	}
	
	public void stop() {
		currentPath = null;
		this.clearAnimateQueue();
		int[] nextMove = {
				this.getBlockX(), this.getBlockY()
		};
		animateQueue.add(nextMove);
	}
	
	public void removePath(World w) {
		boolean check = false;
		if (currentPath != null) {
			int[][] coords = currentPath.getPath();
			for (int i = 0; i < coords.length; i++) {
				if (check == false && coords[i][0] == getBlockX() && coords[i][1] == getBlockY()) {
					check = true;
				}
				if (check || true) {
					w.removePathing(coords[i][0], coords[i][1]);
				}
			}
		}
	}
	
	public boolean pathNeedsUpdate(World w) {
		boolean check = false;
		if (currentPath != null) {
			int[][] coords = currentPath.getPath();
			for (int i = 0; i < coords.length; i++) {
				if (check == false && coords[i][0] == getBlockX() && coords[i][1] == getBlockY()) {
					check = true;
				}
				if (w.isBlocked(coords[i][0], coords[i][1]) && check) {
					return true;
				}
			}
			return false;
		}
		return false;
	}
	
	public boolean equals(Mob other) {
		if (this.getX() == other.getX() && this.getY() == other.getY()) {
			return true;
		}
		return false;
	}
}
