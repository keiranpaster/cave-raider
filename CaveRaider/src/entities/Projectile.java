package entities;

import items.Item;

import java.util.Map;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import Art.GameImages;

import com.keirp.caveraider.Game;

import world.World;

public class Projectile extends Entity {
	
	private Mob target;
	private ItemEffect ie;

	public Projectile(Map<String, Number> dataStore) {
		super(dataStore);
		Game.debug("please dont make me restore projectiles with entity data");
	}
	
	public Projectile(int id, double x, double y, Mob target, float speed) {
		super(id, x, y);
		this.target = target;
		this.setValue("speed", speed);
		this.save(false);
		ie = new ItemEffect(Item.items[id].getAttribute("effect"), target, this);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update(GameContainer gc, World world, int delta) {
		if (target != null) {
			float distX = target.getX() - getX();
			float distY = target.getY() - getY();
			float distMag = (float) Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2));
			if (distMag == 0) {
				ie.done();
				delete();
				return;
			}
			distX /= distMag;
			distY /= distMag;
			distX *= this.getFromStore("speed").floatValue();
			distY *= this.getFromStore("speed").floatValue();
			this.changeX(distX);
			this.changeY(distY);
			this.setRotation(Math.toDegrees(-Math.atan2(distX, distY))  + 90);
		} else {
			delete();
		}
	}

	@Override
	public void render(Graphics g, int x, int y) {
		// TODO Auto-generated method stub
		Image i = Item.getImage(this.getFromStore("id").intValue());
		i.setCenterOfRotation(Item.items[this.getFromStore("id").intValue()].getAttributeAsIntegerList("center")[0] * WORLD_SCALE, Item.items[this.getFromStore("id").intValue()].getAttributeAsIntegerList("center")[0] * WORLD_SCALE);
		i.setRotation(this.getRotation());
		g.drawImage(i, x, y);
	}

}
