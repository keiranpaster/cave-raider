package entities;

import java.util.Map;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.keirp.caveraider.Game;

import world.World;

public abstract class Particle extends Entity {
	
	private int lifespan;
	private long startTime;
	private float vx, vy;

	public Particle(int id, double x, double y, float vx, float vy, int lifespan) {
		super(id, x, y);
		this.vx = vx;
		this.vy = vy;
		this.lifespan = lifespan;
		this.startTime = System.currentTimeMillis();
		// TODO Auto-generated constructor stub
	}

	public Particle(Map<String, Number> dataStore) {
		super(dataStore);
		delete();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update(GameContainer gc, World world, int delta) {
		this.changeX(vx * delta);
		this.changeY(vy * delta);
		if (System.currentTimeMillis() > startTime + lifespan) {
			delete();
		}
	}
	
	public float getProgress() {
		float passedTime = (int) (System.currentTimeMillis() - startTime);
		return passedTime / lifespan;
	}

	@Override
	public abstract void render(Graphics g, int x, int y);

}
