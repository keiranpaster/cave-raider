package entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Play;
import com.keirp.caveraider.Game;

import world.World;

public abstract class Entity implements Commons {
	
	private double[] motion = new double[2];
	private Shape bounds;
	private int offsetX, offsetY;
	private Map<String, Number> dataStoreInteger;
	private boolean shouldMoveChunk = false;
	private boolean shouldSave = true;
	private boolean delete = false;
	
	public Entity(int id, double x, double y) {
		dataStoreInteger = new HashMap<String, Number>();
		dataStoreInteger.put("x", x);
		dataStoreInteger.put("y", y);
		dataStoreInteger.put("id", id);
		dataStoreInteger.put("rotation", 0);
		bounds = new Rectangle(0, 0, 0, 0);
		offsetX = 0;
		offsetY = 0;
	}
	
	public Entity(Map<String, Number> dataStore) {
		dataStoreInteger = dataStore;
		bounds = new Rectangle(0, 0, 0, 0);
		offsetX = 0;
		offsetY = 0;
	}
	
	public void setMoveChunk(boolean b) {
		shouldMoveChunk = b;
	}
	
	public void save(boolean b) {
		shouldSave = b;
	}
	
	public boolean shouldSave() {
		return shouldSave;
	}
	
	public void delete() {
		delete = true;
	}
	
	public boolean shouldDelete() {
		return delete;
	}
	
	public boolean shouldMoveChunk() {
		return shouldMoveChunk;
	}
		
	public int getID() {
		return dataStoreInteger.get("id").intValue();
	}
	
	public int getX() {
		return dataStoreInteger.get("x").intValue();
	}
	
	public int getY() {
		return dataStoreInteger.get("y").intValue();
	}
	
	public int getBlockX() {
		int bx = (int) Math.floor((dataStoreInteger.get("x").intValue() + 1) / BLOCK_SIZE);
		if (dataStoreInteger.get("x").intValue() + 1 < 0) {
			bx--;
		}
		return bx;
	}
	
	public int getBlockY() {
		int by = (int) Math.floor((dataStoreInteger.get("y").intValue() + 1) / BLOCK_SIZE);
		if (dataStoreInteger.get("y").intValue() + 1 < 0) {
			by--;
		}
		return by;
	}
	
	public void setX(Number x) {
		dataStoreInteger.put("x", x);
		bounds.setX(x.floatValue() + offsetX);
	}
	
	public void setY(Number y) {
		dataStoreInteger.put("y", y);
		bounds.setY(y.floatValue() + offsetY);
	}
	
	public void setOffset(int x, int y) {
		this.offsetX = x;
		this.offsetY = y;
	}
	
	public int getOffsetX() {
		return offsetX;
	}
	
	public int getOffsetY() {
		return offsetY;
	}
	
	public void setRotation(Number angle) {
		dataStoreInteger.put("rotation", angle);
		this.bounds = this.bounds.transform(Transform.createRotateTransform((float) Math.toRadians(angle.floatValue()), this.bounds.getCenterX(), this.bounds.getCenterY()));
	}
	
	public float getRotation() {
		return dataStoreInteger.get("rotation").floatValue();
	}

	public void setCenter(int x, int y) {
		this.bounds.setCenterX(x - offsetX);
		this.bounds.setCenterY(y - offsetY);
	}
	
	public void setBounds(Shape b) {
		this.bounds = b;
	}
	
	public Shape getBounds() {
		return bounds;
	}
	
	public double[] getMotion() {
		return motion;
	}

	public void setMotion(double[] motion) {
		this.motion = motion;
	}
	
	public void changeX(Number x) {
		dataStoreInteger.put("x", dataStoreInteger.get("x").doubleValue() + x.doubleValue());
		bounds.setX(getX() + offsetX);
	}
	
	public void changeY(Number y) {
		dataStoreInteger.put("y", dataStoreInteger.get("y").doubleValue() + y.doubleValue());
		bounds.setY(getY() + offsetY);
	}
	
	public void setPosition(double x, double y) {
		dataStoreInteger.put("x", x);
		dataStoreInteger.put("y", y);
	}
	
	public Number getFromStore(String key) {
		return dataStoreInteger.get(key);
	}
		
	public void setValue(String key, Number value) {
		dataStoreInteger.put(key, value);
	}
		
	public abstract void update(GameContainer gc, World world, int delta);
	
	public abstract void render(Graphics g, int x, int y);
	
	public void renderInfo(Graphics g, int x, int y) {
		Play.ttf.drawString(x, y - 20, String.format("%d, %d", this.getBlockX(), this.getBlockY()));
	}
	
	public ArrayList<Attr> getSaveAttributes(Document doc) {
		ArrayList<Attr> attrs = new ArrayList<Attr>();
		Attr classAttr = doc.createAttribute("class");
		Class<?> enclosingClass = getClass().getEnclosingClass();
		if (enclosingClass != null) {
			classAttr.setValue(enclosingClass.getName());
		} else {
			classAttr.setValue(getClass().getName());
		}
		attrs.add(classAttr);
		for (Entry<String, Number> entity : dataStoreInteger.entrySet())
		{
			Attr a = doc.createAttribute(entity.getKey());
			a.setValue(String.valueOf(entity.getValue()));
			attrs.add(a);
		}
		return attrs;
	}
	
}
