package entities;

import items.Item;

import java.util.Map;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import player.PingEffect;

import com.keirp.caveraider.Game;

import Art.GameImages;

import world.MovementIntent;
import world.PathResponse;
import world.ShiftedPath;
import world.World;

public class Bat extends Mob {
	
	private final int FLAP_INT = 500;
	private int px = 0;
	private int py = 0;
	private boolean launch = true;

	public Bat(Map<String, Number> dataStore) {
		super(dataStore);
		this.setMovementSpeed(.01f);
		this.setUpdatePeriod(1000);
	}
	
	public Bat(int id, int x, int y) {
		super(id, x, y);
		this.setHealth(10.0f);
		this.setMovementSpeed(.01f);
		this.setUpdatePeriod(1000);
	}

	@Override
	public void update(GameContainer gc, World world, int delta) {
		final MovementIntent intent;
		final World w = world;
		if ((world.getPlayer().getBlockX() != px || world.getPlayer().getBlockY() != py || pathNeedsUpdate(world) || launch) && canUpdate() && world.distanceBetween(this, world.getPlayer()) < 10) {
			launch = false;
			px = world.getPlayer().getBlockX();
			py = world.getPlayer().getBlockY();
			updatePath();
			 intent = new MovementIntent(world.getPlayer().getBlockX(), world.getPlayer().getBlockY(), 1) {
				@Override
				public void movementFinished() {
					
				}
			};
			try {
				world.getPath(this, World.getBlockXForX(getX()), World.getBlockYForY(getY()), world.getPlayer().getBlockX(), world.getPlayer().getBlockY(),  new PathResponse() {
					@Override
					public void done(ShiftedPath path) {
						if (path != null) {
							pathToQueueWithIntent(path, intent, w);
						} else {
							stop();
						}
					}
				});
			} catch (Exception e) {
				stop();
				//Game.debug("Cannot move to that square.");
			}
		}
		this.animateMovements(delta, world);
		this.spawnEntities(world);
		if (this.isDead()) {
			world.addTileEntity(new Item(12), getBlockX(), getBlockY());
			this.delete();
		}
	}

	@Override
	public void render(Graphics g, int x, int y) {
		int dir = this.getDirection();
		if (dir == 2) {
			dir = 3;
		} else if (dir == 3) {
			dir = 2;
		}
		int flapImg = 44;
		if (System.currentTimeMillis() % FLAP_INT < FLAP_INT / 2) {
			flapImg = 45;
		}
		g.drawImage(GameImages.images[dir][flapImg], x, y);
	}

}
