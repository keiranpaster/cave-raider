package entities;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Game;

public class ItemEffect implements Commons {
	
	private String desc;
	private Mob target;
	private Entity item;
	
	public ItemEffect(String desc, Mob m, Entity e) {
		this.desc = desc;
		this.target = m;
		this.item = e;
	}
	
	public void damage(Mob m, float d) {
		m.changeHealth(-d);
	}
	
	private void effect(char e, float num) {
		switch(e) {
			case 'h':
				target.changeHealth(num);
				break;
		}
	}
	
	public void done() {
		String[] steps = desc.split(";");
		String nums = "0123456789.-";
		String number = "";
		for (String s : steps) {
			for (int i = 0; i < s.length(); i++) {
				if (nums.contains(String.valueOf(s.charAt(i)))) {
					number += s.charAt(i);
				} else {
					effect(s.charAt(i), Float.valueOf(number));
				}
			}
		}
		if (item != null) {
			item.delete();
		}
	}
	
}
