package entities;

import java.util.Map;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class TextParticle extends Particle {

	private String text;
	
	public TextParticle(double x, double y, String text) {
		super(0, x, y, 0, -.01f, 500);
		this.text = text;
		// TODO Auto-generated constructor stub
	}

	public TextParticle(Map<String, Number> dataStore) {
		super(dataStore);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(Graphics g, int x, int y) {
		// TODO Auto-generated method stub
		g.setColor(new Color(1, 0, 0, 1 - (float)Math.sqrt(getProgress())));
		g.drawString(text, x, y);
	}

}
