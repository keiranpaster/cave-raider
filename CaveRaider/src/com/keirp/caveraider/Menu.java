package com.keirp.caveraider;

import org.newdawn.slick.*;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.state.*;

public class Menu extends BasicGameState implements Commons, ComponentListener {
		
	public Menu(int state) {
		
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.drawString("Play Cave Raider", 320, 100);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		Input input = gc.getInput();
		if (input.isKeyDown(Input.KEY_SPACE)) {
			GameData.setWorld("myWorld");
			GameData.setSeed(1002);
			GameData.setGeneration(true);
			GameData.saveData();
			sbg.enterState(1);
		}
	}

	@Override
	public int getID() {
		return 0;
	}

	@Override
	public void componentActivated(AbstractComponent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
