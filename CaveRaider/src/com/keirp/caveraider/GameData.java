package com.keirp.caveraider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GameData implements Commons, Serializable {

	private static final long serialVersionUID = -4814206670345801374L;

	private static GameData data = null;
	
	private String worldName;
	private int worldSeed;
	private boolean shouldGenerate;
	
	public static GameData getData() {
		if (data == null) {
			data = new GameData();
		}
		return data;
	}
	
	public static String getWorldName() {
		if (data == null) {
			data = new GameData();
		}
		return data.worldName;
	}
	
	public static int getSeed() {
		if (data == null) {
			data = new GameData();
		}
		return data.worldSeed;
	}
	
	public static void setWorld(String worldName) {
		if (data == null) {
			data = new GameData();
		}
		File worldFolder = new File(WORLDS_DIRECTORY + "/" + worldName);
		if (!worldFolder.exists()) {
			worldFolder.mkdir();
		}
		data.worldName = worldName;
	}
	
	public static void setSeed(int seed) {
		if (data == null) {
			data = new GameData();
		}
		data.worldSeed = seed;
	}
	
	public static boolean shouldGenerate() {
		if (data == null) {
			data = new GameData();
		}
		return data.shouldGenerate;
	}
	
	public static void setGeneration(boolean shouldGenerate) {
		if (data == null) {
			data = new GameData();
		}
		data.shouldGenerate = shouldGenerate;
	}
	
	public static void saveData() {
		if (data == null) {
			data = new GameData();
		}
		final String filename = WORLDS_DIRECTORY + "/" + data.worldName + "/world.data";
		File gameData = new File(filename);
		if (!gameData.exists()) {
			try {
				FileOutputStream fos = new FileOutputStream(filename);
		        GZIPOutputStream gzos = new GZIPOutputStream(fos);
		        ObjectOutputStream out = new ObjectOutputStream(gzos);
		        out.writeObject(data);
		        out.flush();
		        out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				FileInputStream fis = new FileInputStream(filename);
				GZIPInputStream gzis = new GZIPInputStream(fis);
				ObjectInputStream in = new ObjectInputStream(gzis);
				GameData cf = (GameData)in.readObject();
				data = cf;
				in.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private GameData() {
		
	}
		
}
