package com.keirp.caveraider;

public interface Commons {
	public static final String WORLDS_DIRECTORY = "worlds";
	
	public static final int WORLD_SCALE = 2;
	public static final float ITEM_SCALE = 1.875f / 2 * WORLD_SCALE * .9f;
	public static final float INVENTORY_ITEM_SCALE = 1.875f;
	public static final float PLAYER_HAND_SCALE = .4f * WORLD_SCALE;
	
	public static final int CHUNK_LOAD_DISTANCE = 3;
	public static final int CHUNK_UNLOAD_DISTANCE = 6;
	public static final float AMBIENT_SHADING = 1f;
	public static final int CHUNK_SIZE = 16;
	public static final int BLOCK_SIZE = 16;
	public static final int LIGHTING_SIZE = 15;
	public static final double PLAYER_SPEED = .3f;
	public static final int MAX_STACK_SIZE = 64;
	public static final int MIN_TIME_BETWEEN_PLACE = 200;
	
	public static final int INVENTORY_ROWS = 4;
	public static final int INVENTORY_COLUMNS = 9;
	public static final int INVENTORY_ITEM_SIZE = 30;
	public static final int INVENTORY_SPACING = 20;
	public static final int INVENTORY_WIDTH = 450;
	public static final int SIDE_INVENTORY_SPACING = 10;
	public static final int BOTTOM_INVENTORY_SPACING = 13;
	
	public static final int INVENTORY_HOTBAR_SPACING = 10;
	
	public static final int SELECTED_ITEM_DIFF = 2;
	public static final int STACK_NUMBER_INSET = 11;
	
	public static final int PLAYER_ITEM_SWING_TIME = 200;
	public static final int PLAYER_ITEM_SWING_DISTANCE = 40;
	
	public static final float CRACKS_ALPHA = .7f;
	
	public static final int SCREEN_WIDTH = 800;
	public static final int SCREEN_HEIGHT = 600;
	
	public static final int DEBUG_MODE = 1;
	public static final boolean SHOW_ENTITY_INFO = false;
}