package com.keirp.caveraider;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

import blocks.Block;

import Art.ImageLoader;
import Art.SoundManager;

import player.Camera;
import player.Player;

import world.ChunkCoord;
import world.ChunkManager;
import world.World;

import items.Item;

import java.awt.Font;


public class Play extends BasicGameState implements Commons {
	
	World world;
	Camera camera;
	
	Font font;
	public static TrueTypeFont ttf;
	SoundManager soundManager;
	private boolean hasShown = false;
				
	public Play(int state) {
		
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		gc.setMouseCursor(ImageLoader.get("ui/cursor.png", 1), 0, 0);
		Block.loadBlocks();
		Item.loadItems();
		font = new Font("Verdana", Font.BOLD, 15);
		ttf = new TrueTypeFont(font, true);
		Game.debug("Initializing world");
//		world = new World();
		
		soundManager = new SoundManager();
		
		soundManager.playSongWithID(0, 1, .2f);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		world.render(gc, g);
		
		Input input = gc.getInput();
		//ttf.drawString(10, 30, String.format("Player block position: %d, %d", player.getBlockX(), player.getBlockY()));
//		ttf.drawString(10, 50, String.format("Mouse block position: %d, %d", world.getBlockXForMouseX(input.getMouseX()), world.getBlockYForMouseY(input.getMouseY())));
//		ttf.drawString(10, 70, String.format("Mouse position: %d, %d", world.getXForMouseX(input.getMouseX()), world.getYForMouseY(input.getMouseY())));
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		if (!hasShown) {
			world = new World();
		}
		hasShown = true;
		world.update(gc, delta);
	}
	
	public void saveWorld() {
		world.saveWorld();
	}

	@Override
	public int getID() {
		return 1;
	}
		
}
