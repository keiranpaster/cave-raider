package com.keirp.caveraider;

import java.io.File;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class Game extends StateBasedGame implements Commons {
	
	public static final String gamename = "Cave Raider 1.0";
	public static final int menu = 0;
	public static final int play = 1;
	static AppGameContainer appgc;
	
	public Game(String gamename) {
		super(gamename);
		this.addState(new Menu(menu));
		this.addState(new Play(play));
		this.setupDirectories();
	}
	
	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.getState(menu).init(gc, this);
		this.getState(play).init(gc, this);
		this.enterState(menu);
		
	}

	public static void main(String[] args) {
		try {
			appgc = new AppGameContainer(new Game(gamename));
			appgc.setDisplayMode(SCREEN_WIDTH, SCREEN_HEIGHT, false);
			appgc.setTargetFrameRate(60);
			appgc.setVSync(true);
			appgc.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	public void setupDirectories() {
		File worldsDirectory = new File(WORLDS_DIRECTORY);
		if (!worldsDirectory.exists()) {
			worldsDirectory.mkdir();
		}
	}
	
	@Override
	public boolean closeRequested() {
		Game.debug("Closing game");
		if (this.getCurrentStateID() == 1) {
			((Play)this.getCurrentState()).saveWorld();
		}
		appgc.exit();
		return false;
	}
	
	public static void debug(Object message, int level) {
		if (DEBUG_MODE == level) {
			System.out.println(message);
		}
	}
	
	public static void debug(Object message) {
		if (DEBUG_MODE > 0) {
			System.out.println(message);
		}
	}

}
