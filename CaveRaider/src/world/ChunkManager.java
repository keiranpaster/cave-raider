package world;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.newdawn.slick.GameContainer;

import player.Player;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Game;
import com.keirp.caveraider.GameData;

import entities.Entity;

public class ChunkManager implements Commons {
	
	private Map<ChunkCoord, Chunk> chunks = new HashMap<ChunkCoord, Chunk>(256);
	private PerlinNoiseGenerator worldGenerator;
	
	public ChunkManager(PerlinNoiseGenerator worldGenerator) {
		this.worldGenerator = worldGenerator;
	}
	
	public Map<ChunkCoord, Chunk> getChunks() {
		return chunks;
	}
	
	public void setChunks(Map<ChunkCoord, Chunk> chunks) {
		this.chunks = chunks;
	}

	public Chunk getChunk(int x, int y) {
		if (chunks.containsKey(getChunkKey(x, y))) {
			return chunks.get(getChunkKey(x, y));
		} else {
			return null;
		}
	}
			
	private void unloadChunk(int x, int y) {
		if (chunks.get(getChunkKey(x, y)) != null) {
			chunks.get(getChunkKey(x, y)).saveToFile();
			chunks.remove(getChunkKey(x, y));
			Game.debug("Unloading chunk");
		}
	}
	
	private void loadChunk(int x, int y) {
		if (!chunks.containsKey(getChunkKey(x, y))) {
			Chunk c = new Chunk(x, y);
			if (chunkExists(x, y)) {
				Game.debug("loading chunk from disk");
				c.loadFromFile();
			} else {
				c.generateChunk(worldGenerator);
			}
			chunks.put(getChunkKey(x, y), c);
		}
	}
	
	private boolean chunkExists(int x, int y) {
		final String filename = WORLDS_DIRECTORY + "/" + GameData.getWorldName() + "/" + x + "_" + y + ".chunk";
		File chunk = new File(filename);
		if (chunk.exists()) {
			return true;
		} else {
			return false;
		}
	}
	
	public int convertToChunkX(double x) {
		return (int) Math.floor((float)x / (CHUNK_SIZE * BLOCK_SIZE * WORLD_SCALE));
	}
	
	public int convertToChunkY(double y) {
		return (int) Math.floor((float)y / (CHUNK_SIZE * BLOCK_SIZE * WORLD_SCALE));
	}
	
	public int convertBlockToChunkX(double x) {
		return (int) Math.floor((float)x / (CHUNK_SIZE));
	}
	
	public int convertBlockToChunkY(double y) {
		return (int) Math.floor((float)y / (CHUNK_SIZE));
	}
	
	private ChunkCoord getChunkKey(int x, int y) {
		return new ChunkCoord(x, y);
	}
	
	public int loadedChunkWidth() {
		Integer lowestX = null;
		Integer highestX = null;
		
		Set<ChunkCoord> set = chunks.keySet();
		Iterator<ChunkCoord> itr = set.iterator();
		
		while (itr.hasNext()) {
			ChunkCoord o = itr.next();
			if (lowestX == null) {
				lowestX = o.getX();
			}
			if (highestX == null) {
				highestX = o.getX();
			}
			if (o.getX() > highestX) {
				highestX = o.getX();
			}
			if (o.getX() < lowestX) {
				lowestX = o.getX();
			}
		}
		return Math.abs(highestX - lowestX);
	}
	
	public int lowestChunkX() {
		Integer lowestX = null;
		Set<ChunkCoord> set = chunks.keySet();
		Iterator<ChunkCoord> itr = set.iterator();
		while (itr.hasNext()) {
			ChunkCoord o = itr.next();
			if (lowestX == null) {
				lowestX = o.getX();
			}
			if (o.getX() < lowestX) {
				lowestX = o.getX();
			}
		}
		return lowestX;
	}
	
	public int lowestChunkY() {
		Integer lowestY = null;
		Set<ChunkCoord> set = chunks.keySet();
		Iterator<ChunkCoord> itr = set.iterator();
		while (itr.hasNext()) {
			ChunkCoord o = itr.next();
			if (lowestY == null) {
				lowestY = o.getY();
			}
			if (o.getY() < lowestY) {
				lowestY = o.getY();
			}
		}
		return lowestY;
	}
	
	public int loadedChunkHeight() {
		Integer lowestY = null;
		Integer highestY = null;
		
		Set<ChunkCoord> set = chunks.keySet();
		Iterator<ChunkCoord> itr = set.iterator();
		
		while (itr.hasNext()) {
			ChunkCoord o = itr.next();
			if (lowestY == null) {
				lowestY = o.getY();
			}
			if (highestY == null) {
				highestY = o.getY();
			}
			if (o.getY() > highestY) {
				highestY = o.getY();
			}
			if (o.getY() < lowestY) {
				lowestY = o.getY();
			}
		}
		return Math.abs(highestY - lowestY);
	}
	
	public boolean blockIsLoaded(int x, int y) {
		int chunkX = convertBlockToChunkX(x);
		int chunkY = convertBlockToChunkY(y);
		if (getChunk(chunkX, chunkY) != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public synchronized void update(Player player, GameContainer gc, World world, int delta) {
		int playerChunkX = convertBlockToChunkX(player.getBlockX());
		int playerChunkY = convertBlockToChunkY(player.getBlockY());
		for (int i = playerChunkX - CHUNK_LOAD_DISTANCE; i < playerChunkX + CHUNK_LOAD_DISTANCE; i++) {
			for (int c = playerChunkY - 2; c < playerChunkY + 2; c++) {
				loadChunk(i, c);
				getChunk(i, c).update(gc, world, delta, player);
				ArrayList<Entity> me = getChunk(i, c).getMovingEntities();
				for (Entity e : me) {
					transferEntity(e, getChunk(i, c), getChunk(convertBlockToChunkX(e.getBlockX()), convertBlockToChunkY(e.getBlockY())));
					e.setMoveChunk(false);
				}
			}
		}
		
		Set<ChunkCoord> set = chunks.keySet();
		Iterator<ChunkCoord> itr = set.iterator();
		
		ArrayList<ChunkCoord> unloadingChunks = new ArrayList<ChunkCoord>();
		while (itr.hasNext()) {
			ChunkCoord o = itr.next();
			if (Math.abs(playerChunkX - o.getX()) > CHUNK_UNLOAD_DISTANCE || Math.abs(playerChunkY - o.getY()) > CHUNK_UNLOAD_DISTANCE) {
				unloadingChunks.add(o);
			}
		}
		
		for (ChunkCoord cc : unloadingChunks) {
			unloadChunk(cc.getX(), cc.getY());
		}
	}
	
	public synchronized void saveChunks() {
		Set<ChunkCoord> set = chunks.keySet();
		Iterator<ChunkCoord> itr = set.iterator();
		
		while (itr.hasNext()) {
			ChunkCoord o = itr.next();
			chunks.get(o).saveToFile();
		}
	}
	
	private void transferEntity(Entity e, Chunk c1, Chunk c2) {
		c1.removeEntity(e);
		c2.addEntity(e);
	}
	
}
