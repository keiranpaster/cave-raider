package world;

import java.util.ArrayList;

import blocks.Block;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Game;

public class ShadowCastingLighter implements Runnable, Commons {
	
	int updateX, updateY;
	PositiveWorld map;
	int mapWidth, mapHeight;
	boolean add;

	public ShadowCastingLighter(PositiveWorld map, int x, int y, boolean add) {
		this.add = add;
		this.updateX = x;
		this.updateY = y;
		this.map = map;
		mapWidth = map.getWidthInTiles() - 1;
		mapHeight = map.getHeightInTiles() - 1;
	}
	
	@Override
	public void run() {
		if (add) {
			if (Block.blocks[map.getBlockID(updateX, updateY)].isLightSource()) {
				addLighting(updateX, updateY);
			} else {
				refreshLighting(updateX, updateY);
			}
		} else {
			refreshLighting(updateX, updateY);
		}
	}
	
	protected void refreshLighting(int x, int y) {
		ArrayList<Light> lights = map.getLights(x, y);
		for (int i = 0; i < lights.size(); i++) {
			removeLighting(lights.get(i).getX() - map.getBlockOffsetX(), lights.get(i).getY() - map.getBlockOffsetY());
//			addLighting(l.getX() + map.getBlockOffsetX(), l.getY() + map.getBlockOffsetY());
		}
	}
	
	protected void removeLighting(int x, int y) {
		for (int i = 0; i < map.getWidthInTiles(); i++) {
			for (int c = 0; c < map.getHeightInTiles(); c++) {
				map.removeLight(i, c, x, y);
			}
		}
	}
	
	protected void addLighting(int x, int y) {
		
		ArrayList<Point> sight = new ArrayList<Point>();
;
		
		for (int octant = 1; octant < 9; octant++)
        {
            scan(sight, x, y, 1, octant, 1.0, 0.0);
        }
		
		ArrayList<Point> blockingSight = new ArrayList<Point>();
		
		for (int c = 0; c < mapHeight; c++) {
			for (int i = 0; i < mapWidth; i++) {
				Point p = new Point(i, c);
				if (sight.contains(new Point(i + 1, c + 1))) {
					if (!blockingSight.contains(p)) {
						blockingSight.add(p);
					}
				}
				if (sight.contains(new Point(i - 1, c - 1))) {
					if (!blockingSight.contains(p)) {
						blockingSight.add(p);
					}
				}
				if (sight.contains(new Point(i + 1, c - 1))) {
					if (!blockingSight.contains(p)) {
						blockingSight.add(p);
					}
				}
				if (sight.contains(new Point(i - 1, c + 1))) {
					if (!blockingSight.contains(p)) {
						blockingSight.add(p);
					}
				}
			}
		}
		
		sight.addAll(blockingSight);
		
		for (int c = 0; c < mapHeight; c++) {
			for (int i = 0; i < mapWidth; i++) {
				if (i == x && c == y) {
					Game.debug("%");
					map.addLight(LIGHTING_SIZE, i, c);
				} else {
					if (sight.contains(new Point(i, c))) {
						map.addLight(getLightIntensity(x, y, i, c), i, c);
						Game.debug("#");
					} else {
						Game.debug(".");
					}
				}
			}
		}
	}
	
	protected void scan(ArrayList<Point> sight, int startX, int startY, int _depth, int _octant, double _startSlope, double _endSlope)
    {
				
        int x = 0;
        int y = 0;
        
        switch (_octant)
        {
            case 1:
                y = startY - _depth;
                x = startX - (int)(_startSlope * _depth);

                if (x < 0) break;
                if (x >= mapWidth) break;
                if (y < 0) break;
                if (y >= mapHeight) break;

                while (getSlope(x, y, startX, startY) >= _endSlope)
                {
                    if (isVisible(startX, startY, x, y))
                    {

                        if (map.blocked(null, x, y)) //cell blocked
                        {
                            //if prior open AND within range
                            if (testCell(startX, startY, x - 1, y, false, _depth))
                            {
                                //recursion
                                scan(sight, startX, startY, _depth + 1, _octant, _startSlope, getSlope(x - .5, y + 0.5, startX, startY));
                            }

                        }
                        else //not blocked
                        {
                            //if prior closed AND within range
                            if (testCell(startX, startY, x - 1, y, true, _depth))
                            {
                                _startSlope = getSlope(x - .5, y - 0.5, startX, startY);
                            }
                            sight.add(new Point(x, y));
                        }

                    }
                    x++;

                }
                x--; //we step back as the last step of the while has taken us past the limit

                break;

            case 2:

                y = startY - _depth;
                x = startX + (int)(_startSlope * _depth);

                if (x < 0) break;
                if (x >= mapWidth) break;
                if (y < 0) break;
                if (y >= mapHeight) break;

                while (getSlope(x, y, startX, startY) <= _endSlope)
                {
                    if (isVisible(startX, startY, x, y))
                    {

                        if (map.blocked(null, x, y))
                        {
                            if (testCell(startX, startY, x + 1, y, false, _depth))
                            {
                                scan(sight, startX, startY, _depth + 1, _octant, _startSlope, getSlope(x + 0.5, y + 0.5, startX, startY));
                            }
                        }
                        else
                        {
                            if (testCell(startX, startY, x + 1, y, true, _depth))
                            {
                                _startSlope = -getSlope(x + 0.5, y - 0.5, startX, startY);
                            }
                            sight.add(new Point(x, y));
                        }

                    }
                    x--;

                }
                x++;

                break;


            case 3:

                x = startX + _depth;
                y = startY - (int)(_startSlope * _depth);

                if (x < 0) break;
                if (x >= mapWidth) break;
                if (y < 0) break;
                if (y >= mapHeight) break;

                while (getSlopeInv(x, y, startX, startY) <= _endSlope)
                {

                    if (isVisible(startX, startY, x, y))
                    {

                        if (map.blocked(null, x, y)) //cell blocked
                        {
                            //if prior open AND within range
                            if (testCell(startX, startY, x, y - 1, false, _depth))
                            {
                                scan(sight, startX, startY, _depth + 1, _octant, _startSlope, getSlopeInv(x - 0.5, y - 0.5, startX, startY));
                            }
                        }
                        else //not blocked
                        {
                            //if prior closed AND within range
                            if (testCell(startX, startY, x, y - 1, true, _depth))
                            {
                                _startSlope = -getSlopeInv(x + 0.5, y - 0.5, startX, startY);
                            }
                            sight.add(new Point(x, y));
                        }

                    }
                    y++;

                }
                y--; //we step back as the last step of the while has taken us past the limit

                break;

            case 4:

                x = startX + _depth;
                y = startY + (int)(_startSlope * _depth);

                if (x < 0) break;
                if (x >= mapWidth) break;
                if (y < 0) break;
                if (y >= mapHeight) break;;

                while (getSlopeInv(x, y, startX, startY) >= _endSlope)
                {

                    if (isVisible(startX, startY, x, y))
                    {

                        if (map.blocked(null, x, y))
                        {
                            if (testCell(startX, startY, x, y + 1, false, _depth))
                            {
                                scan(sight, startX, startY, _depth + 1, _octant, _startSlope, getSlopeInv(x - 0.5, y + 0.5, startX, startY));
                            }
                        }
                        else
                        {

                            if (testCell(startX, startY, x, y + 1, true, _depth))
                            {
                                _startSlope = getSlopeInv(x + 0.5, y + 0.5, startX, startY);
                            }
                            sight.add(new Point(x, y));
                        }

                    }
                    y--;

                }
                y++;

                break;

            case 5:

                y = startY + _depth;
                x = startX + (int)(_startSlope * _depth);

                if (x < 0) break;
                if (x >= mapWidth) break;
                if (y < 0) break;
                if (y >= mapHeight) break;

                while (getSlope(x, y, startX, startY) >= _endSlope)
                {
                    if (isVisible(startX, startY, x, y))
                    {

                        if (map.blocked(null, x, y))
                        {
                            if (testCell(startX, startY, x + 1, y, false, _depth))
                            {
                                scan(sight, startX, startY, _depth + 1, _octant, _startSlope, getSlope(x + 0.5, y - 0.5, startX, startY));
                            }
                        }
                        else
                        {
                            if (testCell(startX, startY, x + 1, y, true, _depth))
                            {
                                _startSlope = getSlope(x + 0.5, y + 0.5, startX, startY);
                            }
                            sight.add(new Point(x, y));
                        }

                    }
                    x--;

                }
                x++;

                break;

            case 6:

                y = startY + _depth;
                x = startX - (int)(_startSlope * _depth);

                if (x < 0) break;
                if (x >= mapWidth) break;
                if (y < 0) break;
                if (y >= mapHeight) break;

                while (getSlope(x, y, startX, startY) <= _endSlope)
                {
                    if (isVisible(startX, startY, x, y))
                    {

                        if (map.blocked(null, x, y))
                        {
                            if (testCell(startX, startY, x - 1, y, false, _depth))
                            {
                                scan(sight, startX, startY, _depth + 1, _octant, _startSlope, getSlope(x - 0.5, y - 0.5, startX, startY));
                            }
                        }
                        else
                        {
                            if (testCell(startX, startY, x - 1, y, true, _depth))
                            {
                                _startSlope = -getSlope(x - 0.5, y + 0.5, startX, startY);
                            }
                            sight.add(new Point(x, y));
                        }

                    }
                    x++;

                }
                x--;

                break;

            case 7:

                x = startX - _depth;
                y = startY + (int)(_startSlope * _depth);

                if (x < 0) break;
                if (x >= mapWidth) break;
                if (y < 0) break;
                if (y >= mapHeight) break;

                while (getSlopeInv(x, y, startX, startY) <= _endSlope)
                {

                    if (isVisible(startX, startY, x, y))
                    {

                        if (map.blocked(null, x, y))
                        {
                            if (testCell(startX, startY, x, y + 1, false, _depth))
                            {
                                scan(sight, startX, startY, _depth + 1, _octant, _startSlope, getSlopeInv(x + 0.5, y + 0.5, startX, startY));
                            }
                        }
                        else
                        {
                            if (testCell(startX, startY, x, y + 1, true, _depth))
                            {
                                _startSlope = getSlopeInv(x - 0.5, y + 0.5, startX, startY);
                            }
                            sight.add(new Point(x, y));
                        }

                    }
                    y--;

                }
                y++;
                break;

            case 8:

                x = startX - _depth;
                y = startY - (int)(_startSlope * _depth);

                if (x < 0) break;
                if (x >= mapWidth) break;
                if (y < 0) break;
                if (y >= mapHeight) break;

                while (getSlopeInv(x, y, startX, startY) >= _endSlope)
                {

                    if (isVisible(startX, startY, x, y))
                    {

                        if (map.blocked(null, x, y))
                        {
                            if (testCell(startX, startY, x, y - 1, false, _depth))
                            {
                                scan(sight, startX, startY, _depth + 1, _octant, _startSlope, getSlopeInv(x + 0.5, y - 0.5, startX, startY));
                            }
                        }
                        else
                        {
                            if (testCell(startX, startY, x, y - 1, true, _depth))
                            {
                                _startSlope = getSlopeInv(x - 0.5, y - 0.5, startX, startY);
                            }
                            sight.add(new Point(x, y));
                        }

                    }
                    y++;

                }
                y--;

                break;


        }


        if (x < 0) x = 0;
        if (x >= mapWidth) x = mapWidth - 1;
        if (y < 0) y = 0;
        if (y >= mapHeight) y = mapHeight - 1;

        if (isVisible(startX, startY, x, y) && !map.blocked(null, x, y))
        {
            scan(sight, startX, startY, _depth + 1, _octant, _startSlope, _endSlope);
        }


    }
	
	protected boolean isVisible(int _x1, int _y1, int _x2, int _y2)
    {
        if (map.contains(_x1, _y1) && map.contains(_x2, _y2))
        {
//            boolean i = map.blocked(null, _x1, _y1);  //illegal values throw an error
//            i = map.blocked(null, _x2, _y2);
        	
        	

            if (_x1 == _x2) //if they're on the same axis, we only need to test one
            //value, which is computationally cheaper than what we do below
            {
                return Math.abs(_y1 - _y2) <= LIGHTING_SIZE;
            }

            if (_y1 == _y2)
            {
                return Math.abs(_x1 - _x2) <= LIGHTING_SIZE;
            }

            return (Math.pow((_x1 - _x2), 2) + Math.pow((_y1 - _y2), 2)) <= Math.pow(LIGHTING_SIZE, 2);
        }
        else
        {
            return false;
        }
    }
	
	private double getSlope(double _x1, double _y1, double _x2, double _y2)
    {
        return (_x1 - _x2) / (_y1 - _y2);
    }

    private double getSlopeInv(double _x1, double _y1, double _x2, double _y2)
    {
        return (_y1 - _y2) / (_x1 - _x2);
    }
    
    private boolean testCell(int startX, int startY, int _x, int _y, boolean _cellState, int _depth)
    {
        try
        {
            if (!isVisible(startX, startY, _x, _y)) throw new Exception();
            return map.blocked(null, _x, _y) == _cellState;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    
    private int getLightIntensity(int startX, int startY, int x, int y) {
    	double distance = Math.sqrt(Math.pow(x - startX, 2) + Math.pow(y - startY, 2));
    	double invsqr = 1 / distance;
    	return LIGHTING_SIZE - (int)distance;
    }
    
    private class Point {
    	
    	int x, y;
    	
    	public Point(int x, int y) {
    		this.x = x;
    		this.y = y;
    	}
    	
    	@Override
		public boolean equals(Object other) {
			return (this.x == ((Point) other).x && this.y == ((Point) other).y);
		}
    }
	
}
