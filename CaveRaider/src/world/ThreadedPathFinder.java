package world;

import javax.swing.SwingWorker;

import org.newdawn.slick.util.pathfinding.AStarPathFinder;
import org.newdawn.slick.util.pathfinding.Mover;
import org.newdawn.slick.util.pathfinding.Path;
import org.newdawn.slick.util.pathfinding.TileBasedMap;
import org.newdawn.slick.util.pathfinding.heuristics.ManhattanHeuristic;

public class ThreadedPathFinder extends SwingWorker<Path, Path> {
	
	AStarPathFinder pf;
	Mover mover;
	int sx, sy, fx, fy;
	
	public ThreadedPathFinder(TileBasedMap world, Mover mover, int sx, int sy, int fx, int fy) {
		pf = new AStarPathFinder(world, 100, false, new ManhattanHeuristic(1));
		this.mover = mover;
		this.sx = sx;
		this.sy = sy;
		this.fx = fx;
		this.fy = fy;
		if (world.blocked(null, fx, fy)) {
			correctFinishCoordinates();
		}
	}
	
	public void correctFinishCoordinates() {
		int[][] deltaCoords = {
				{
					1,
					0
				},
				{
					-1,
					0
				},
				{
					0,
					1
				},
				{
					0,
					-1
				}
		};
		boolean hasPath = false;
		int shortestPath = 0;
		int nfx = fx, nfy = fy;
		for (int i = 0; i < deltaCoords.length; i++) {
			if (sx == fx + deltaCoords[i][0] && sy == fy + deltaCoords[i][1]) {
				nfx = fx + deltaCoords[i][0];
				nfy = fy + deltaCoords[i][1];
				shortestPath = 0;
				break;
			} else {
				Path p = pf.findPath(mover, sx, sy, fx + deltaCoords[i][0], fy + deltaCoords[i][1]);
				if (p != null) {
					if (!hasPath || p.getLength() < shortestPath) {
						hasPath = true;
						shortestPath = p.getLength();
						nfx = fx + deltaCoords[i][0];
						nfy = fy + deltaCoords[i][1];
					}
				}
			}
		}
		fx = nfx;
		fy = nfy;
	}

	@Override
	protected Path doInBackground() throws Exception {
		if (sx == fx && sy == fy) {
			Path p = new Path();
			p.appendStep(sx, sy);
			return p;
		}
		try {
			Path p = pf.findPath(mover, sx, sy, fx, fy);
			return p;
		} catch (Exception e) {
			throw e;
		}
	}

}
