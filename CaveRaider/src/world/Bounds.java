package world;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Bounds {
	
	int[][] boundsCoords;
	float rotation;
	int centerX, centerY;
	
	public Bounds(int width, int height) {
		int[][] rectBounds = {
				{
					0,
					0
				},
				{
					width,
					0
				},
				{
					width,
					height
				},
				{
					0,
					height
				}
		};
		this.setBounds(rectBounds);
		this.setCenter(0, 0);
	}
	
	private void setBounds(int[][] bounds) {
		this.boundsCoords = bounds;
	}
	
	public void setCenter(int x, int y) {
		this.centerX = x;
		this.centerY = y;
	}
	
	public void setRotation(float rotation) {
		rotation = (float) (Math.PI * rotation / 180);
	}
	
	public int[][] getBounds() {
		int[][] bounds = new int[boundsCoords.length][2];
		for (int i = 0; i < bounds.length; i++) {
			bounds[i][0] = (int) (Math.cos(rotation) * (boundsCoords[i][0] - centerX) - Math.sin(rotation) * ((boundsCoords[i][1] - centerY) + centerX));
			bounds[i][1] = (int) (Math.sin(rotation) * (boundsCoords[i][0] - centerX) + Math.cos(rotation) * ((boundsCoords[i][1] - centerY) + centerY));
		}
		return bounds;
	}
	
//	public boolean isCollidingWith(Bounds b) {
//
//	}
	
}
