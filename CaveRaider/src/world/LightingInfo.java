package world;

import java.io.Serializable;
import java.util.ArrayList;

public class LightingInfo implements Serializable, Cloneable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6379229172102345480L;
	private ArrayList<Light> lights = new ArrayList<Light>();
	private int lightingLevel = 0;

	public LightingInfo() {

	}
	
	public void addLight(Light lc) {
		boolean contains = false;
		for (int i = 0; i < lights.size(); i++) {
			if (lights.get(i).getX() == lc.getX() && lights.get(i).getY() == lc.getY() && lights.get(i).getChunkCoord() == lc.getChunkCoord()) {
				contains = true;
				lights.get(i).setLightLevel(lc.getLightLevel());
				break;
			}
		}
		if (!contains) {
			lights.add(0, lc);
		}
		
		calculateHighestLightingValue();
	}
	
	public void removeLight(Light lc) {
		lights.remove(lc);
		calculateHighestLightingValue();
	}
	
	public void removeLight(ChunkCoord cc, int x, int y) {
		if (this.getHighestLightingValue() > 0) {
			for (int i = 0; i < lights.size(); i++) {
				if (lights.get(i).getX() == x && lights.get(i).getY() == y && lights.get(i).getChunkCoord().equals(cc)) {
					lights.remove(lights.get(i));
				}
			}
			calculateHighestLightingValue();
		}
	}
	
	public int getLightLevelForCoords(ChunkCoord cc, int x, int y) throws Exception {
		for (int i = 0; i < lights.size(); i++) {
			if (lights.get(i).getX() == x && lights.get(i).getY() == y && lights.get(i).getChunkCoord() == cc) {
				return lights.get(i).getLightLevel();
			}
		}
		
		throw new Exception();
	}
	
	private void calculateHighestLightingValue() {
		int highestValue = 0;
		for (int i = 0; i < lights.size(); i++) {
			if (lights.get(i) != null && lights.get(i).getLightLevel() > highestValue) {
				highestValue = lights.get(i).getLightLevel();
			}
		}
		lightingLevel =  highestValue; 
	}
	
	public int getHighestLightingValue() {
		return lightingLevel;
	}
	
	public Light getHighestLight() throws Exception {
		int highestValue = 0;
		Light highestLight = null;
		for (int i = 0; i < lights.size(); i++) {
			if (lights.get(i).getLightLevel() > highestValue) {
				highestValue = lights.get(i).getLightLevel();
				highestLight = lights.get(i);
			}
		}
		if (highestLight != null) {
			return highestLight; 
		} else {
			throw new Exception();
		}	
	}
	
	public ArrayList<Light> getLights() {
		return lights;
	}
	
}
