package world;

import java.util.ArrayList;

import org.newdawn.slick.util.pathfinding.PathFindingContext;
import org.newdawn.slick.util.pathfinding.TileBasedMap;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Game;

import entities.Mob;

import blocks.Block;

class PositiveWorld implements TileBasedMap, Commons {
		
		private ChunkManager cm;
		private int blockOffsetX;
		private int blockOffsetY;
		private Mob mob;
		
		public PositiveWorld(ChunkManager cm) {
			this.cm  = cm;
			blockOffsetX = cm.lowestChunkX() * CHUNK_SIZE;
			blockOffsetY = cm.lowestChunkY() * CHUNK_SIZE;
		}
		
		public PositiveWorld(ChunkManager cm, Mob m) {
			this.mob = m;
			this.cm  = cm;
			blockOffsetX = cm.lowestChunkX() * CHUNK_SIZE;
			blockOffsetY = cm.lowestChunkY() * CHUNK_SIZE;
		}

		@Override
		public boolean blocked(PathFindingContext ctx, int x, int y) {
			if (cm.blockIsLoaded((x + blockOffsetX), (y + blockOffsetY))) {
				boolean hasMob = cm.getChunk(cm.convertBlockToChunkX((x + blockOffsetX)), cm.convertBlockToChunkY((y + blockOffsetY))).hasMob(x + blockOffsetX, y + blockOffsetY);
				boolean hasPath = false;
				if (mob != null) {
					hasPath = cm.getChunk(cm.convertBlockToChunkX((x + blockOffsetX)), cm.convertBlockToChunkY((y + blockOffsetY))).hasPathing(mob, x % CHUNK_SIZE, y % CHUNK_SIZE);
				}
				return !Block.blocks[cm.getChunk(cm.convertBlockToChunkX((x + blockOffsetX)), cm.convertBlockToChunkY((y + blockOffsetY))).getBlocksArray()[x % CHUNK_SIZE][y % CHUNK_SIZE]].isTransparent() || hasMob || hasPath;
			} else {
				return true;
			}
		}

		@Override
		public float getCost(PathFindingContext ctx, int x, int y) {
			return 1;
		}

		@Override
		public int getHeightInTiles() {
			// TODO Auto-generated method stub
			return (cm.loadedChunkHeight() + 1) * CHUNK_SIZE;
		}

		@Override
		public int getWidthInTiles() {
			// TODO Auto-generated method stub
			return (cm.loadedChunkWidth() + 1) * CHUNK_SIZE;
		}

		@Override
		public void pathFinderVisited(int arg0, int arg1) {
			// TODO Auto-generated method stub
			
		}

		public int getBlockOffsetX() {
			return blockOffsetX;
		}

		public int getBlockOffsetY() {
			return blockOffsetY;
		}
		
		public boolean contains(int x, int y) {
			if (x >= 0 && x < this.getWidthInTiles() && y >= 0 && y < this.getHeightInTiles()) {
				return true;
			} else {
				return false;
			}
		}
		
		public void addLight(int intensity, int x, int y) {
			int realX, realY;
			ChunkCoord cc;
			realX = x + blockOffsetX;
			realY = y + blockOffsetY;
			cc = new ChunkCoord(cm.convertBlockToChunkX(realX), cm.convertBlockToChunkY(realY));
			Chunk c = cm.getChunk(cc.getX(), cc.getY());
			int innerX = realX - (cc.getX() * CHUNK_SIZE);
			int innerY = realY - (cc.getY() * CHUNK_SIZE);
			c.getLightingGrid()[innerX][innerY].addLight(new Light(cc, realX, realY, intensity));
		}
		
		public void removeLight(int x, int y, int x1, int y1) {
			int realX, realY;
			ChunkCoord cc;
			realX = x + blockOffsetX;
			realY = y + blockOffsetY;
			cc = new ChunkCoord(cm.convertBlockToChunkX(realX), cm.convertBlockToChunkY(realY));
			Chunk c = cm.getChunk(cc.getX(), cc.getY());
			int innerX = realX - (cc.getX() * CHUNK_SIZE);
			int innerY = realY - (cc.getY() * CHUNK_SIZE);
			
			int lrealX, lrealY;
			ChunkCoord lcc;
			lrealX = x1 + blockOffsetX;
			lrealY = y1 + blockOffsetY;
			lcc = new ChunkCoord(cm.convertBlockToChunkX(lrealX), cm.convertBlockToChunkY(lrealY));
			int linnerX = lrealX - (lcc.getX() * CHUNK_SIZE);
			int linnerY = lrealY - (lcc.getY() * CHUNK_SIZE);
			
			c.getLightingGrid()[innerX][innerY].removeLight(lcc, linnerX, linnerY);
		}
		
		public int getBlockID(int x, int y) {
			int realX, realY;
			ChunkCoord cc;
			realX = x + blockOffsetX;
			realY = y + blockOffsetY;
			cc = new ChunkCoord(cm.convertBlockToChunkX(realX), cm.convertBlockToChunkY(realY));
			Chunk c = cm.getChunk(cc.getX(), cc.getY());
			int innerX = realX - (cc.getX() * CHUNK_SIZE);
			int innerY = realY - (cc.getY() * CHUNK_SIZE);
			return c.getBlocksArray()[innerX][innerY];
		}
		
		public ArrayList<Light> getLights(int x, int y) {
			int realX, realY;
			ChunkCoord cc;
			realX = x + blockOffsetX;
			realY = y + blockOffsetY;
			cc = new ChunkCoord(cm.convertBlockToChunkX(realX), cm.convertBlockToChunkY(realY));
			Chunk c = cm.getChunk(cc.getX(), cc.getY());
			int innerX = realX - (cc.getX() * CHUNK_SIZE);
			int innerY = realY - (cc.getY() * CHUNK_SIZE);
			return c.getLightingGrid()[innerX][innerY].getLights();
		}

	}