package world;

import java.io.Serializable;

public class ChunkCoord implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6931584962786834725L;
	private int x, y;
	
	public ChunkCoord(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	@Override
	public String toString() {
		return x + "_" + y;
	}
	
	@Override
	public boolean equals(Object other) {
		return (this.x == ((ChunkCoord) other).x && this.y == ((ChunkCoord) other).y);
	}
	
	@Override
	public int hashCode() {
		int hash = 1;
		hash = hash * 31 + x;
		hash = hash * 31 + y;
		return hash;
	}
	
}
