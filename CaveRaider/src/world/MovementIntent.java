package world;

import com.keirp.caveraider.Game;

public abstract class MovementIntent {
	
	private int x, y, radius;

	public MovementIntent(int x, int y, int radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	public boolean shouldStopMoving(int x, int y, World world) {
		if ((this.x == x && this.y == y) || Math.sqrt(Math.pow(Math.abs(x - this.x), 2) + Math.pow(Math.abs(y - this.y), 2)) < radius && world.lineOfSight(x, y, this.x, this.y)) {
			return true;
		} else {
			return false;
		}
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public abstract void movementFinished();
	
}
