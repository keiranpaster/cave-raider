package world;

public abstract class PathResponse {

	public abstract void done(ShiftedPath path);
	
}
