package world;

import org.newdawn.slick.util.pathfinding.Path;

public class ShiftedPath {
	
	int[][] path;

	public ShiftedPath(Path path, int ox, int oy) {
		this.path = new int[path.getLength()][2];
		for (int i = 0; i < path.getLength(); i++) {
			this.path[i][0] = path.getX(i) + ox;
			this.path[i][1] = path.getY(i) + oy;
		}
	}
	
	public int[][] getPath() {
		return path;
	}
	
	public int getLength() {
		return path.length;
	}
	
	public int getX(int index) {
		return path[index][0];
	}
	
	public int getY(int index) {
		return path[index][1];
	}
	
	public boolean contains(int[] move) {
		for (int i = 0; i < path.length; i++) {
			if (path[i][0] == move[0] && path[i][1] == move[1]) {
				return true;
			}
		}
		return false;
	}
	
	public int getPositionOf(int[] move) throws Exception {
		for (int i = 0; i < path.length; i++) {
			if (path[i][0] == move[0] && path[i][1] == move[1]) {
				return i + 1;
			}
		}
		throw new Exception();
	}
	
	public void removePathUpTo(int index) {
		int[][] tempPath = new int[path.length - index][2];
		for (int i = index; i < path.length; i++) {
			tempPath[i - index] = path[i];
		}
		path = tempPath;
	}
	
	public boolean equals(ShiftedPath other) {
		int[][] otherPath = other.getPath(); 
		for (int i = 0; i < path.length; i++) {
			if (path[i][0] != otherPath[i][0] || path[i][1] != otherPath[i][1]) {
				return false;
			}
		}
		return true;
	}
	
}
