package world;

import java.io.*;
import java.lang.reflect.Constructor;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import player.Player;

import Art.GameImages;
import blocks.Block;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Game;
import com.keirp.caveraider.GameData;

import entities.Entity;
import entities.Mob;
import entities.TileEntity;
import items.Item;

public class Chunk implements Commons {
	
	private int[][] blocksArray = new int[CHUNK_SIZE][CHUNK_SIZE];
	private int[][] damageArray = new int[CHUNK_SIZE][CHUNK_SIZE];
	private Mob[][] pathArray = new Mob[CHUNK_SIZE][CHUNK_SIZE];
	private LightingInfo[][] lightingGrid = new LightingInfo[CHUNK_SIZE][CHUNK_SIZE];
	private boolean[][] visibleGrid = new boolean[CHUNK_SIZE][CHUNK_SIZE];
	private ArrayList<Entity> entities = new ArrayList<Entity>();
	private int x, y;
	private boolean chunkShouldSave;
	
	private ArrayList<int[]> endpoints;
	
	private static int[][] crackImages = {
		{
			0,
			0
		},
		{
			1,
			0
		},
		{
			2,
			0
		},
		{
			3,
			0
		},
		{
			4,
			0
		},
		{
			5,
			0
		},
		{
			0,
			1
		},
		{
			1,
			1
		},
		{
			2,
			1
		},
		{
			3,
			1
		}
	};

	public Chunk(int x, int y) {
		chunkShouldSave = true;
		this.x = x;
		this.y = y;
		endpoints = new ArrayList<int[]>();
	}
	
	public synchronized void loadFromFile() {
		final String filename = WORLDS_DIRECTORY + "/" + GameData.getWorldName() + "/" + x + "_" + y + ".chunk";
		try {
			synchronized (lightingGrid) {
				File f = new File(filename);
				Document dom = null;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				dom = db.parse(f);
				dom.getDocumentElement().normalize();
				ChunkFile cf = new ChunkFile();
				this.blocksArray = cf.getBlocksArray(dom);
				this.lightingGrid = cf.getLightingGrid(dom);
				this.entities = cf.getEntities(dom);
				this.damageArray = cf.getDamageArray(dom);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Game.debug("Loading chunk");
				
	}
	
	private class ChunkFile {
		
		int[][] blocksArray;
		LightingInfo[][] lightingGrid;
		ArrayList<Entity> entities;
		int[][] damageArray;
		
		public ChunkFile(int[][] blocksArray, int[][] damageArray, LightingInfo[][] lightingGrid, ArrayList<Entity> entities) {
			this.blocksArray = blocksArray;
			this.lightingGrid = lightingGrid;
			this.entities = entities;
			this.damageArray = damageArray;
		}
		
		public ChunkFile() {
			
		}
		
		public int[][] getBlocksArray(Document dom) {
			int[][] blocksArray = new int[CHUNK_SIZE][CHUNK_SIZE];
			Element chunkEle = dom.getDocumentElement();
			NodeList blocksList = chunkEle.getElementsByTagName("blocks");
			Element blocksEle = (Element)blocksList.item(0);
			NodeList blocks = blocksEle.getElementsByTagName("block");
			if (blocks != null && blocks.getLength() > 0) {
				int x = 0, y = 0;
				for (int i = 0; i < blocks.getLength(); i++) {
					Element block = (Element)blocks.item(i);
					int blockID = Integer.parseInt(block.getAttribute("id"));
					blocksArray[x][y] = blockID;
					y++;
					if (y >= CHUNK_SIZE) {
						y = 0;
						x++;
					}
				}
			}
			return blocksArray;
		}
		
		public int[][] getDamageArray(Document dom) {
			int[][] damageArray = new int[CHUNK_SIZE][CHUNK_SIZE];
			Element chunkEle = dom.getDocumentElement();
			NodeList blocksList = chunkEle.getElementsByTagName("blocks");
			Element blocksEle = (Element)blocksList.item(0);
			NodeList blocks = blocksEle.getElementsByTagName("block");
			if (blocks != null && blocks.getLength() > 0) {
				int x = 0, y = 0;
				for (int i = 0; i < blocks.getLength(); i++) {
					Element block = (Element)blocks.item(i);
					int blockID = Integer.parseInt(block.getAttribute("damage"));
					damageArray[x][y] = blockID;
					y++;
					if (y >= CHUNK_SIZE) {
						y = 0;
						x++;
					}
				}
			}
			return damageArray;
		}
		
		public LightingInfo[][] getLightingGrid(Document dom) {
			LightingInfo[][] lightingGrid = new LightingInfo[CHUNK_SIZE][CHUNK_SIZE];
			Element chunkEle = dom.getDocumentElement();
			NodeList lighting = chunkEle.getElementsByTagName("lighting");
			Element lightingEle = (Element)lighting.item(0);
			NodeList infos = lightingEle.getElementsByTagName("info");
			if (infos != null && infos.getLength() > 0) {
				int x = 0, y = 0;
				for (int i = 0; i < infos.getLength(); i++) {
					Element info = (Element)infos.item(i);
					NodeList lights = info.getElementsByTagName("light");
					lightingGrid[x][y] = new LightingInfo();
					if (lights != null && lights.getLength() > 0) {
						for (int c = 0; c < lights.getLength(); c++) {
							Element light = (Element)lights.item(c);
							int blockX = 0, blockY = 0, chunkX = 0, chunkY = 0, lightLevel = 0;
							try {
								String[] block = light.getAttribute("block").split(",");
								int[] r = new int[block.length];
								for (int d = 0; d < block.length; d++) {
									r[d] = Integer.parseInt(block[d]);
								}
								blockX = r[0];
								blockY = r[1];
								String[] chunk = light.getAttribute("chunk").split(",");
								for (int d = 0; d < chunk.length; d++) {
									r[d] = Integer.parseInt(chunk[d]);
								}
								chunkX = r[0];
								chunkY = r[1];
								lightLevel = Integer.parseInt(light.getAttribute("level"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							lightingGrid[x][y].addLight(new Light(new ChunkCoord(chunkX, chunkY), blockX, blockY, lightLevel));
						}
					}
					y++;
					if (y >= CHUNK_SIZE) {
						y = 0;
						x++;
					}
				}
			}
			return lightingGrid;
		}
		
		public ArrayList<Entity> getEntities(Document dom) {
			ArrayList<Entity> entityArrayList = new ArrayList<Entity>();
			Element chunkEle = dom.getDocumentElement();
			NodeList entityList = chunkEle.getElementsByTagName("entities");
			Element entitiesEle = (Element)entityList.item(0);
			NodeList entities = entitiesEle.getElementsByTagName("entity");
			if (entities != null && entities.getLength() > 0) {
				for (int i = 0; i < entities.getLength(); i++) {
					Element entity = (Element)entities.item(i);
					Map<String, Number> dataStore = new HashMap<String, Number>();
					NamedNodeMap attrs = entity.getAttributes();
					for (int c = 0; c < attrs.getLength(); c++) {
						if (!attrs.item(c).getNodeName().equals("class")) {
							
							try {
								dataStore.put(attrs.item(c).getNodeName(), NumberFormat.getInstance(Locale.US).parse(attrs.item(c).getNodeValue()));
							} catch (DOMException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					Entity ent = null;
					try {
						Class<?> entityClass = Class.forName(entity.getAttribute("class"));
						Class<?>[] types = {Map.class};
						Constructor<?> constructor = entityClass.getConstructor(types);
						Object[] parameters = {
							dataStore
						};
						ent = (Entity) constructor.newInstance(parameters);
					} catch (Exception e) {
						e.printStackTrace();
					}
					entityArrayList.add(ent);
				}
			}
			return entityArrayList;
		}
		
		public Document getXMLDocument() {
			Document doc = null;
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				doc = docBuilder.newDocument();
				Element rootElement = doc.createElement("chunk");
				doc.appendChild(rootElement);
				
				// blocks
				Element blocksList = doc.createElement("blocks");
				for (int i = 0; i < blocksArray.length; i++) {
					for (int c = 0; c < blocksArray[i].length; c++) {
						Element block = doc.createElement("block");
						block.setAttribute("id", String.valueOf(blocksArray[i][c]));
						block.setAttribute("damage", String.valueOf(damageArray[i][c]));
						blocksList.appendChild(block);
					}
				}
				rootElement.appendChild(blocksList);
				
				// lighting
				Element lighting = doc.createElement("lighting");
				for (int i = 0; i < lightingGrid.length; i++) {
					for (int c = 0; c < lightingGrid[i].length; c++) {
						Element info = doc.createElement("info");
						for (Light l : lightingGrid[i][c].getLights()) {
							Element light = doc.createElement("light");
							light.setAttribute("block", l.getX() + "," + l.getY());
							light.setAttribute("chunk", l.getChunkCoord().getX() + "," + l.getChunkCoord().getY());
							light.setAttribute("level", String.valueOf(l.getLightLevel()));
							info.appendChild(light);
						}
						lighting.appendChild(info);
					}
				}
				rootElement.appendChild(lighting);
				
				// entities
				Element entitiesList = doc.createElement("entities");
				for (Entity e : entities) {
					if (e.shouldSave()) {
						Element entity = doc.createElement("entity");
						ArrayList<Attr> attrs = e.getSaveAttributes(doc);
						for (Attr a : attrs) {
							entity.setAttributeNode(a);
						}
						entitiesList.appendChild(entity);
					}
				}
				rootElement.appendChild(entitiesList);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return doc;
		}
		
	}
	
	public synchronized void saveToFile() {
		if (chunkShouldSave) {
			 String filename = WORLDS_DIRECTORY + "/" + GameData.getWorldName() + "/" + x + "_" + y + ".chunk";
			try {
				synchronized (lightingGrid) {
					ChunkFile cf = new ChunkFile(blocksArray, damageArray, lightingGrid, entities);
			        Source s = new DOMSource(cf.getXMLDocument());
			        File f = new File(filename);
			        Result r = new StreamResult(f);
			        Transformer xformer = TransformerFactory.newInstance().newTransformer();
			        xformer.transform(s, r);
				}
		     } catch (Exception e) {
		         e.printStackTrace();
		     }
			chunkShouldSave = false;
		}
	}
	
	public void generateChunk(PerlinNoiseGenerator generator) {
		Game.debug("Generating new chunk: " + x + ", " + y);
		int xOffset = x * CHUNK_SIZE, yOffset = y * CHUNK_SIZE;
		
		// GENERATE RANDOM TUNNELS FROM SEED
		if (GameData.shouldGenerate()) {
			for (int i = 0; i < CHUNK_SIZE; i++) {
				for (int c = 0; c < CHUNK_SIZE; c++) {
					float turbulence = generator.turbulence2(i + xOffset + .5f, c + yOffset + .5f, .1f) * 100;
					if ((int)(turbulence) > 0) {
//						Game.debug(turbulence);
//						if (turbulence > 300f) {
//							blocksArray[i][c] = 3;
//						} else {
							blocksArray[i][c] = 1;
//						}
						damageArray[i][c] = Block.blocks[1].getMaxHealth();
					} else {
						blocksArray[i][c] = 0;
						damageArray[i][c] = 0;
					}
				}
			}
		}
		
		for (int i = 0; i < CHUNK_SIZE; i++) {
			for (int c = 0; c < CHUNK_SIZE; c++) {
				lightingGrid[i][c] = new LightingInfo();
			}
		}
	}
	
	public int[][] getBlocksArray() {
		return blocksArray;
	}
	
	public int[][] getDamageArray() {
		return damageArray;
	}
	
	public int getCrackValue(int x, int y) {
		int maxHealth = Block.blocks[blocksArray[x][y]].getMaxHealth();
		int currentHealth = damageArray[x][y];
		return 10 - (int) Math.ceil(((float)currentHealth/maxHealth) * 10);
	}
	
	public LightingInfo[][] getLightingGrid() {
		return lightingGrid;
	}
	
	public void setLightingGrid(LightingInfo[][] lightingGrid) {
		this.lightingGrid = lightingGrid;
	}

	public void render(GameContainer gc, Graphics g, int cameraX, int cameraY, Player p) {		
		int offsetX = getRealX(x), offsetY = getRealY(y);
		for (int i = 0; i < blocksArray.length; i++) {
			for (int c = 0; c < blocksArray[i].length; c++) {
				if (Block.blocks[blocksArray[i][c]] != null && blocksArray[i][c] != 0 && lightingGrid[i][c].getHighestLightingValue() != 0) {
					Block.blocks[blocksArray[i][c]].render(g, i * BLOCK_SIZE * WORLD_SCALE - cameraX + offsetX, c * BLOCK_SIZE * WORLD_SCALE - cameraY + offsetY);
					int crackValue = getCrackValue(i, c);
					if (crackValue > 0) {
						crackValue--;
						GameImages.images[crackImages[crackValue][0]][crackImages[crackValue][1]].setAlpha(CRACKS_ALPHA);
						g.drawImage(GameImages.images[crackImages[crackValue][0]][crackImages[crackValue][1]], i * BLOCK_SIZE * WORLD_SCALE - cameraX + offsetX, c * BLOCK_SIZE * WORLD_SCALE - cameraY + offsetY);
					}
				}
			}
		}
		
		for (int i = 0; i < entities.size(); i++) {
			entities.get(i).render(g, entities.get(i).getX() * WORLD_SCALE - cameraX, entities.get(i).getY() * WORLD_SCALE - cameraY);
			if (entities.get(i) == p.getTargetMob()) {
				g.setColor(new Color(1f, 0f, 0f, .7f));
				g.drawOval(entities.get(i).getX() * WORLD_SCALE - cameraX, entities.get(i).getY() * WORLD_SCALE - cameraY, BLOCK_SIZE * WORLD_SCALE, BLOCK_SIZE * WORLD_SCALE);
			} else if (entities.get(i) == p.getHoverMob()) {
				g.setColor(new Color(1f, 0f, 0f, .4f));
				g.drawOval(entities.get(i).getX() * WORLD_SCALE - cameraX, entities.get(i).getY() * WORLD_SCALE - cameraY, BLOCK_SIZE * WORLD_SCALE, BLOCK_SIZE * WORLD_SCALE);
			}
			if (SHOW_ENTITY_INFO) {
				entities.get(i).renderInfo(g, entities.get(i).getX() * WORLD_SCALE - cameraX, entities.get(i).getY() * WORLD_SCALE - cameraY);
			}
		}
	}
	
	public void renderLighting(GameContainer gc, Graphics g, int cameraX, int cameraY) {
		int offsetX = getRealX(x), offsetY = getRealY(y);
		for (int i = 0; i < blocksArray.length; i++) {
			for (int c = 0; c < blocksArray[i].length; c++) {
				float shading = 1 - lightingGrid[i][c].getHighestLightingValue() * 1f / LIGHTING_SIZE;
				if (shading > AMBIENT_SHADING) {
					shading = AMBIENT_SHADING;
				}
				if (!visibleGrid[i][c]) {
					shading = 1f;
				}
				g.setColor(new Color(0f, 0f, 0f, shading));
				g.fillRect(i * BLOCK_SIZE * WORLD_SCALE - cameraX + offsetX, c * BLOCK_SIZE * WORLD_SCALE - cameraY + offsetY, BLOCK_SIZE * WORLD_SCALE, BLOCK_SIZE * WORLD_SCALE);
			}
		}
	}
	
	public float getAlphaForBlock(LightingInfo l) {
		return 1 - (float)Math.pow(.85, LIGHTING_SIZE - l.getHighestLightingValue());
	}
	
	public Color getFillForAlpha(float a) {
		return new Color(0f, 0f, 0f, a);
	}
	
	public int testForEdge(int i, int c, int l, int w) {
		if (i < 1) {
			return 1;
		} else if (i > l - 1) {
			return 2;
		} else {
			return 0;
		}
	}
	
	public void addEntity(Entity e) {
		entities.add(e);
	}
	
	public void addPathing(Mob m, int x, int y) {
		pathArray[x][y] = m;
	}
	
	public void removePathing(int x, int y) {
		pathArray[x][y] = null;
	}
	
	public boolean hasPathing(Mob m, int x, int y) {
		return pathArray[x][y] != null && !pathArray[x][y].equals(m);
	}
	
	public boolean hasPathing(int x, int y) {
		return pathArray[x][y] != null;
	}
	
	public void removeEntity(Entity e) {
		entities.remove(e);
	}
	
	public boolean hasTileEntity(int x, int y) {
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			if (e.getBlockX() == x && e.getBlockY() == y && e instanceof TileEntity) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasMob(int x, int y) {
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			if (e.getBlockX() == x && e.getBlockY() == y && e instanceof Mob) {
				return true;
			}
		}
		return false;
	}
	
	public Entity getEntity(int x, int y) {
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			if (e.getBlockX() == x && e.getBlockY() == y) {
				return e;
			}
		}
		return null;
	}
	
	public ArrayList<Entity> getEntities() {
		ArrayList<Entity> e = new ArrayList<Entity>();
		for (int i = 0; i < entities.size(); i++) {
			e.add(entities.get(i));
		}
		return e;
	}
	
	public int numberOfMobs(int x, int y) {
		int num = 0;
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			if (e.getBlockX() == x && e.getBlockY() == y && e instanceof Mob) {
				num++;
			}
		}
		return num;
	}
	
	public boolean hasEndpoint(int x, int y) {
		Game.debug("YAY0" + endpoints.size());
		if (endpoints.size() == 0) {
			return false;
		}
		for (int i = 0; i < endpoints.size(); i++) {
			if (endpoints.get(i)[0] == x && endpoints.get(i)[1] == y) {
				return true;
			}
		}
		return false;
	}
	
	public void addEndpoint(int[] endpoint) {
		if (!endpoints.contains(endpoint)) {
			endpoints.add(endpoint);
		}
	}
	
	public void removeEndpoint(int[] endpoint) {
		endpoints.remove(endpoint);
	}
	
	public int getTileEntityID(int x, int y) {
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			if (e.getBlockX() == x && e.getBlockY() == y && e instanceof TileEntity) {
				return e.getID();
			}
		}
		return 0;
	}
	
	public int getItemID(int x, int y) {
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			if (e.getBlockX() == x && e.getBlockY() == y && e instanceof Item) {
				return ((Item)e).getItemID();
			}
		}
		return 0;
	}
	
	public void removeTileEntity(int x, int y) {
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			if (e.getBlockX() == x && e.getBlockY() == y && e instanceof TileEntity) {
				this.removeEntity(e);
			}
		}
	}
	
	public float getAverage(float a, float b) {
		return (a + b) / 2;
	}
	
	public void update(GameContainer gc, World world, int delta, Player p) {
		for (int i = 0; i < blocksArray.length; i++) {
			for (int c = 0; c < blocksArray[i].length; c++) {
				if (Block.blocks[blocksArray[i][c]] != null && blocksArray[i][c] != 0) {
					Block.blocks[blocksArray[i][c]].update(gc, delta);
				}
//				visibleGrid[i][c] = world.lineOfSight(p.getBlockX(), p.getBlockY(), i + x * CHUNK_SIZE, c + y * CHUNK_SIZE);
				visibleGrid[i][c] = Math.sqrt(Math.pow(p.getBlockX() - (i + x * CHUNK_SIZE), 2) + Math.pow(p.getBlockY() - (c + y * CHUNK_SIZE), 2)) < 7;
			}
		}
		
		for (int i = 0; i < entities.size(); i++) {
			entities.get(i).update(gc, world, delta);
			
			if (!contains(entities.get(i).getBlockX(), entities.get(i).getBlockY())) {
				entities.get(i).setMoveChunk(true);
			}
			if (entities.get(i).shouldDelete()) {
				entities.remove(i);
				i--;
			}
		}
	}
	
	public void chunkUpdate() {
		chunkShouldSave = true;
	}
	
	private int getRealX(int x) {
		return x * CHUNK_SIZE * BLOCK_SIZE * WORLD_SCALE;
	}
	
	private int getRealY(int y) {
		return y * CHUNK_SIZE * BLOCK_SIZE * WORLD_SCALE;
	}
	
	public boolean contains(int x, int y) {
		if (x > this.x * CHUNK_SIZE && x < this.x * (CHUNK_SIZE + 1)) {
			if (y > this.y * CHUNK_SIZE && y < this.y * (CHUNK_SIZE + 1)) {
				return true;
			}
		}
		return false;
	}
	
	public ArrayList<Entity> getMovingEntities() {
		ArrayList<Entity> movingEntities = new ArrayList<Entity>();
		for (Entity e : entities) {
			if (e.shouldMoveChunk()) {
				movingEntities.add(e);
			}
		}
		return movingEntities;
	}
	
}