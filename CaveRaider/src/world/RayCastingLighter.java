package world;

import java.util.ArrayList;
import java.util.Map;

import javax.swing.SwingWorker;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Game;

public class RayCastingLighter extends SwingWorker<Map<ChunkCoord, Chunk>, Map<ChunkCoord, Chunk>> implements Commons {
	
	Light updateLight;
	World world;
	PositiveWorld pw;
	ArrayList<BlockCoord> blockCoords = new ArrayList<BlockCoord>();
	
	public RayCastingLighter(World world, Light updateLight) {
		this.updateLight = updateLight;
		this.world = world;
	}

	private BlockCoord getBlockCoordsForAngle(int angle, int distance, int x, int y) {
		int blockX, blockY;
		blockX = (int) Math.floor(Math.cos(angle * Math.PI / 180) * distance) + x;
		blockY = (int) Math.floor(Math.sin(angle * Math.PI / 180) * distance) + y;
		return new BlockCoord(blockX, blockY);
	}
	
	private class BlockCoord {
		
		int x, y;
		
		public BlockCoord(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}
		
		@Override
		public String toString() {
			return x + "," + y;
		}

		@Override
		public boolean equals(Object other) {
			return (this.x == ((BlockCoord) other).x && this.y == ((BlockCoord) other).y);
		}		
	}

	@Override
	protected Map<ChunkCoord, Chunk> doInBackground() throws Exception {
		pw = world.getPositiveWorld();
		for (int i = 0; i < 360; i++) {
			BlockCoord bc = this.getBlockCoordsForAngle(i, LIGHTING_SIZE, updateLight.getRealBlockX() - pw.getBlockOffsetX(), updateLight.getRealBlockY() - pw.getBlockOffsetY());
			if (!blockCoords.contains(bc)) {
				blockCoords.add(bc);
			}
		}
		Game.debug("hi" + blockCoords.size());
		for (int i = 0; i < blockCoords.size(); i++) {
			castLight(updateLight.getRealBlockX(), updateLight.getRealBlockY(), blockCoords.get(i).getX(), blockCoords.get(i).getY(), true, updateLight);
		}
		Game.debug(blockCoords.size());
		return null;
	}
	
	private int getSign(int number) {
		if (number < 0) {
			return -1;
		} else {
			return 1;
		}
	}
	
	private void castLight(int endX, int endY, int startX, int startY, boolean add, Light light) {
		
		
		startX -= pw.getBlockOffsetX();
		startY -= pw.getBlockOffsetY();
		endX -= pw.getBlockOffsetX();
		endY -= pw.getBlockOffsetY();
		
		try {
			int t, x, y, ax, ay, sx, sy, dx, dy;
			
			dx = startX - endX;
			dy = startY - endY;
			
			ax = Math.abs(dx) << 1;
			ay = Math.abs(dy) << 1;
			
			sx = getSign(dx);
			sy = getSign(dy);
			
			x = endX;
			y = endY;
			
			if (ax > ay) {
				t = ay - (ax >> 1);
				do {
					if (t >= 0) {
						y += sy;
						t -= ax;
					}
					x += sx;
					t += ay;
					ChunkManager cm = world.getChunkManager();
					ChunkCoord cc = new ChunkCoord(cm.convertBlockToChunkX(x  + pw.getBlockOffsetX()), cm.convertBlockToChunkY(y  + pw.getBlockOffsetY()));
					int blockX = x - (cc.getX() * CHUNK_SIZE) + pw.getBlockOffsetX();
					int blockY = y - (cc.getY() * CHUNK_SIZE) + pw.getBlockOffsetY();
					Map<ChunkCoord, Chunk> cmap = cm.getChunks();
					Chunk c = cmap.get(cc);
					LightingInfo[][] lg = c.getLightingGrid();
					lg[blockX % CHUNK_SIZE][blockY % CHUNK_SIZE].addLight(updateLight);
									
				} while (!pw.blocked(null, x, y));
			} else {
				t = ax - (ay >> 1);
				do {
					if (t >= 0) {
						x += sx;
						t -= ay;
					}
					y += sy;
					t += ax;
					ChunkManager cm = world.getChunkManager();
					ChunkCoord cc = new ChunkCoord(cm.convertBlockToChunkX(x  + pw.getBlockOffsetX()), cm.convertBlockToChunkY(y  + pw.getBlockOffsetY()));
					int blockX = x - (cc.getX() * CHUNK_SIZE) + pw.getBlockOffsetX();
					int blockY = y - (cc.getY() * CHUNK_SIZE) + pw.getBlockOffsetY();
					Map<ChunkCoord, Chunk> cmap = cm.getChunks();
					Chunk c = cmap.get(cc);
					LightingInfo[][] lg = c.getLightingGrid();
					lg[blockX % CHUNK_SIZE][blockY % CHUNK_SIZE].addLight(updateLight);
				} while (!pw.blocked(null, x, y));
			}
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
