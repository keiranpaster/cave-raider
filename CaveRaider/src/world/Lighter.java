package world;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.SwingWorker;

import blocks.Block;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Game;

public class Lighter extends SwingWorker<Map<ChunkCoord, Chunk>, Map<ChunkCoord, Chunk>> implements Commons {
	
	private Map<ChunkCoord, Chunk> chunks;
	private ChunkCoord cc;
	private int bx, by;
	private boolean add, transparent;
	
	private Set<ChunkCoord> chunkUpdates = new HashSet<ChunkCoord>();
		
	public Lighter(Map<ChunkCoord, Chunk> chunks, ChunkCoord chunkPos, int blockX, int blockY, boolean add, boolean transparent) {
		this.chunks = chunks;
		this.cc = chunkPos;
		this.bx = blockX;
		this.by = blockY;
		this.add = add;
		this.transparent = !transparent;
	}
	
	@Override
	protected Map<ChunkCoord, Chunk> doInBackground() throws Exception {
		if (transparent) {
			if (add) {
				addLighting(cc, bx, by);
			} else {
				removeLighting(cc, bx, by);
			}
		} else {
			synchronized (chunks) {
				refreshLighting(cc, bx, by);
			}
		}

		for (ChunkCoord cc : chunkUpdates) {
			chunks.get(cc).chunkUpdate();
		}
		
		return chunks;
		
	}
	
	private void removeLighting(ChunkCoord lightChunk, int lightX, int lightY) {
		Game.debug("Removing lighting with arguements: " + lightChunk + ", " + lightX + ", " + lightY + ".");
		for (int i = -LIGHTING_SIZE + 1; i <= LIGHTING_SIZE - 1; i++) {
			int DIAMOND_HEIGHT_MAX = LIGHTING_SIZE - Math.abs(i);
			for (int c = -DIAMOND_HEIGHT_MAX; c < DIAMOND_HEIGHT_MAX; c++) {
				ChunkCoord chunk;
				if (lightX + i > CHUNK_SIZE - 1) {
					if (lightY + c > CHUNK_SIZE - 1) {
						chunk = new ChunkCoord(lightChunk.getX() + 1, lightChunk.getY() + 1);
					} else if (lightY + c < 0) {
						chunk = new ChunkCoord(lightChunk.getX() + 1, lightChunk.getY() - 1);
					} else {
						chunk = new ChunkCoord(lightChunk.getX() + 1, lightChunk.getY());
					}
				} else if (lightX + i < 0) {
					if (lightY + c > CHUNK_SIZE - 1) {
						chunk = new ChunkCoord(lightChunk.getX() - 1, lightChunk.getY() + 1);
					} else if (lightY + c < 0) {
						chunk = new ChunkCoord(lightChunk.getX() - 1, lightChunk.getY() - 1);
					} else {
						chunk = new ChunkCoord(lightChunk.getX() - 1, lightChunk.getY());
					}
				} else  if (lightY + c > CHUNK_SIZE - 1) {
					chunk = new ChunkCoord(lightChunk.getX(), lightChunk.getY() + 1);
				} else if (lightY + c < 0) {
					chunk = new ChunkCoord(lightChunk.getX(), lightChunk.getY() - 1);
				} else {
					chunk = new ChunkCoord(lightChunk.getX(), lightChunk.getY());
				}
				
				LightingInfo[][] lg = chunks.get(chunk).getLightingGrid();
				int blockX, blockY;
				blockX = (lightX + i) % CHUNK_SIZE;
				blockY = (lightY + c) % CHUNK_SIZE;
				if (blockX < 0)
					blockX += CHUNK_SIZE;
				if (blockY < 0)
					blockY += CHUNK_SIZE;
				synchronized (lg) {
					lg[blockX][blockY].removeLight(lightChunk, lightX, lightY);
				}
				chunkUpdates.add(chunk);
			}
		}
	}
	
	private void refreshLighting(ChunkCoord lightChunk, int lightX, int lightY) {
		ArrayList<Light> lights = chunks.get(cc).getLightingGrid()[bx][by].getLights();		
		for (int i = 0; i < lights.size(); i++) {
			Light l = lights.get(i);
			removeLighting(lights.get(i).getChunkCoord(), lights.get(i).getX(), lights.get(i).getY());
			addLighting(l.getChunkCoord(), l.getX(), l.getY());
		}

		
	}
		
	private void addLighting(ChunkCoord lightChunk, int lightX, int lightY) {
		chunks.get(lightChunk).getLightingGrid()[lightX][lightY].addLight(new Light(lightChunk, lightX, lightY, LIGHTING_SIZE));
		spreadLightingAroundBlock(lightChunk, lightX, lightY, lightChunk, lightX, lightY, LIGHTING_SIZE, new ArrayList<String>());
	}
	
	private void spreadLightingAroundBlock(ChunkCoord chunk, int x, int y, ChunkCoord lightChunk, int lightX, int lightY, int pastLightingLevel, ArrayList<String> visited) {
		
		try {
			
			chunkUpdates.add(chunk);
			
			visited.add(chunk.toString() + "_" + x + "_" + y);
			LightingInfo[][] lg = chunks.get(chunk).getLightingGrid();
			int[][] ba = chunks.get(chunk).getBlocksArray();
			
			int lightingLevel = (pastLightingLevel > 1) ? pastLightingLevel - 1 : 0;
			boolean spreadToEmptyBlocks = true;
					
			if (ba[x][y] != 0 && !Block.blocks[ba[x][y]].isTransparent()) {
				lightingLevel = 0;
				spreadToEmptyBlocks = false;
			}
			
			if (lightingLevel < 1) {
				lightingLevel = 0;
			}
			
			int northBlock;
			int southBlock;
			int eastBlock;
			int westBlock;
			
			if (lightingLevel > 0) {
				if (y > 0) {
					northBlock = ba[x][y - 1];
					if (spreadToEmptyBlocks || (northBlock != 0 && !Block.blocks[northBlock].isTransparent())) {
						if (!visited.contains(chunk.toString() + "_" + x + "_" + (y - 1)) || lg[x][y - 1].getLightLevelForCoords(lightChunk, lightX, lightY) < lightingLevel) {
							synchronized (lg) {
								lg[x][y - 1].addLight(new Light(lightChunk,
										lightX, lightY, lightingLevel));
								spreadLightingAroundBlock(chunk, x, y - 1,
										lightChunk, lightX, lightY,
										lightingLevel, visited);
							}
						}
						
					}
				} else {
					ChunkCoord nc = new ChunkCoord(chunk.getX(), chunk.getY() - 1);
					int[][] nba = chunks.get(nc).getBlocksArray();
					LightingInfo[][] nlg = chunks.get(nc).getLightingGrid();
					northBlock = nba[x][CHUNK_SIZE - 1];
					if (spreadToEmptyBlocks || (northBlock != 0 && !Block.blocks[northBlock].isTransparent())) {
						if (!visited.contains(nc.toString() + "_" + x + "_" + (CHUNK_SIZE - 1)) || nlg[x][CHUNK_SIZE - 1].getLightLevelForCoords(lightChunk, lightX, lightY) < lightingLevel) {
							synchronized (nlg) {
								nlg[x][CHUNK_SIZE - 1].addLight(new Light(
										lightChunk, lightX, lightY,
										lightingLevel));
								spreadLightingAroundBlock(nc, x,
										CHUNK_SIZE - 1, lightChunk, lightX,
										lightY, lightingLevel, visited);
							}
						}
					}
				}
				
				if (y < CHUNK_SIZE - 1) {
					southBlock = ba[x][y + 1];
					if (spreadToEmptyBlocks || (southBlock != 0 && !Block.blocks[southBlock].isTransparent())) {
						if (!visited.contains(chunk.toString() + "_" + x + "_" + (y + 1)) || lg[x][y + 1].getLightLevelForCoords(lightChunk, lightX, lightY) < lightingLevel) {
							synchronized (lg) {
								lg[x][y + 1].addLight(new Light(lightChunk,
										lightX, lightY, lightingLevel));
								spreadLightingAroundBlock(chunk, x, y + 1,
										lightChunk, lightX, lightY,
										lightingLevel, visited);
							}
						}
					}
				} else {
					ChunkCoord nc = new ChunkCoord(chunk.getX(), chunk.getY() + 1);
					int[][] nba = chunks.get(nc).getBlocksArray();
					LightingInfo[][] nlg = chunks.get(nc).getLightingGrid();
					southBlock = nba[x][0];
					if (spreadToEmptyBlocks || (southBlock != 0 && !Block.blocks[southBlock].isTransparent())) {
						if (!visited.contains(nc.toString() + "_" + x + "_" + (0)) || nlg[x][0].getLightLevelForCoords(lightChunk, lightX, lightY) < lightingLevel) {
							synchronized (nlg) {
								nlg[x][0].addLight(new Light(lightChunk,
										lightX, lightY, lightingLevel));
								spreadLightingAroundBlock(nc, x, 0, lightChunk,
										lightX, lightY, lightingLevel, visited);
							}
						}
					}
				}
				
				if (x < CHUNK_SIZE - 1) {
					eastBlock = ba[x + 1][y];
					if (spreadToEmptyBlocks || (eastBlock != 0 && !Block.blocks[eastBlock].isTransparent())) {
						if (!visited.contains(chunk.toString() + "_" + (x + 1) + "_" + y) || lg[x + 1][y].getLightLevelForCoords(lightChunk, lightX, lightY) < lightingLevel) {
							synchronized (lg) {
								lg[x + 1][y].addLight(new Light(lightChunk,
										lightX, lightY, lightingLevel));
								spreadLightingAroundBlock(chunk, x + 1, y,
										lightChunk, lightX, lightY,
										lightingLevel, visited);
							}
						}
					}
				} else {
					ChunkCoord nc = new ChunkCoord(chunk.getX() + 1, chunk.getY());
					int[][] nba = chunks.get(nc).getBlocksArray();
					LightingInfo[][] nlg = chunks.get(nc).getLightingGrid();
					eastBlock = nba[0][y];
					if (spreadToEmptyBlocks || (eastBlock != 0 && !Block.blocks[eastBlock].isTransparent())) {
						if (!visited.contains(nc.toString() + "_" + (0) + "_" + y) || nlg[0][y].getLightLevelForCoords(lightChunk, lightX, lightY) < lightingLevel) {
							synchronized (nlg) {
								nlg[0][y].addLight(new Light(lightChunk,
										lightX, lightY, lightingLevel));
								spreadLightingAroundBlock(nc, 0, y, lightChunk,
										lightX, lightY, lightingLevel, visited);
							}
						}
					}
				}
				
				if (x > 0) {
					westBlock = ba[x - 1][y];
					if (spreadToEmptyBlocks || (westBlock != 0 && !Block.blocks[westBlock].isTransparent())) {
						if (!visited.contains(chunk.toString() + "_" + (x - 1) + "_" + y) || lg[x - 1][y].getLightLevelForCoords(lightChunk, lightX, lightY) < lightingLevel) {
							synchronized (lg) {
								lg[x - 1][y].addLight(new Light(lightChunk,lightX, lightY, lightingLevel));
								spreadLightingAroundBlock(chunk, x - 1, y, lightChunk, lightX, lightY, lightingLevel, visited);
							}
						}
					}
				} else {
					ChunkCoord nc = new ChunkCoord(chunk.getX() - 1, chunk.getY());
					int[][] nba = chunks.get(nc).getBlocksArray();
					LightingInfo[][] nlg = chunks.get(nc).getLightingGrid();
					westBlock = nba[CHUNK_SIZE - 1][y];
					if (spreadToEmptyBlocks || (westBlock != 0 && !Block.blocks[westBlock].isTransparent())) {
						if (!visited.contains(nc.toString() + "_" + (CHUNK_SIZE - 1) + "_" + y) || nlg[CHUNK_SIZE - 1][y].getLightLevelForCoords(lightChunk, lightX, lightY) < lightingLevel) {
							synchronized (nlg) {
								nlg[CHUNK_SIZE - 1][y].addLight(new Light(
										lightChunk, lightX, lightY,
										lightingLevel));
								spreadLightingAroundBlock(nc, CHUNK_SIZE - 1,
										y, lightChunk, lightX, lightY,
										lightingLevel, visited);
							}
						}
 					}
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		
		
	}
		
}
