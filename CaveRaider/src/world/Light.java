package world;

import java.io.Serializable;

import com.keirp.caveraider.Commons;

public class Light implements Serializable, Commons {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1475575841316577178L;
	private int x, y, lightLevel;
	private ChunkCoord cc;
	
	public Light(ChunkCoord cc, int x, int y, int lightLevel) {
		this.x = x;
		this.y = y;
		this.cc = cc;
		this.lightLevel = lightLevel;
	}
	
	public Light(ChunkCoord cc, int x, int y) {
		this.x = x;
		this.y = y;
		this.cc = cc;
		this.lightLevel = 0;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public ChunkCoord getChunkCoord() {
		return cc;
	}
	
	public int getLightLevel() {
		return lightLevel;
	}
	
	public void setLightLevel(int ll) {
		this.lightLevel = ll;
	}
	
	@Override
	public String toString() {
		return x + "_" + y;
	}
	
	public int getRealBlockX() {
		return cc.getX() * CHUNK_SIZE + x; 
	}
	
	public int getRealBlockY() {
		return cc.getY() * CHUNK_SIZE + y; 
	}
	
}
