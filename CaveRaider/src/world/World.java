package world;

import items.Item;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

import player.Player;

import Art.GameImages;

import blocks.Block;

import com.keirp.caveraider.Commons;
import com.keirp.caveraider.Game;
import com.keirp.caveraider.GameData;

import entities.Entity;
import entities.Mob;
import entities.TileEntity;

public class World implements Commons {

	Image dirtBackground;
	
	private int cameraX;
	private int cameraY;
	private Player player;
	
	private boolean lightingUpdate, transparentUpdate, add;
	private Light updateLight;
	
	private PerlinNoiseGenerator worldGenerator;
	
	private ChunkManager chunkManager;
	
 	private int runningLightings;
 		
	public World() {
		
		runningLightings = 0;
		
		dirtBackground = GameImages.images[0][6];
		
		Game.debug("Seed is: " + GameData.getWorldName());
		worldGenerator = new PerlinNoiseGenerator(GameData.getSeed());
		chunkManager = new ChunkManager(worldGenerator);
		
		cameraX = 0;
		cameraY = 0;
		player = new Player();
		
	}
	
	public void addEndpoint(int x, int y) {
		int[] endpoint = {
				x, y
		};
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		chunkManager.getChunk(cc.getX(), cc.getY()).addEndpoint(endpoint);
	}
	
	public void removeEndpoint(int x, int y) {
		int[] endpoint = {
				x, y
		};
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		chunkManager.getChunk(cc.getX(), cc.getY()).removeEndpoint(endpoint);
	}
	
	public boolean addTileEntity(TileEntity te, int x, int y) {
		te.setBlockPosition(x, y);
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		boolean hasTileEntity = chunkManager.getChunk(cc.getX(), cc.getY()).hasTileEntity(x, y);
		x = x - (cc.getX() * CHUNK_SIZE);
		y = y - (cc.getY() * CHUNK_SIZE);
		if (chunkManager.getChunk(cc.getX(), cc.getY()).getBlocksArray()[x][y] != 0 || hasTileEntity) {
			return false;
		} else {
			chunkManager.getChunk(cc.getX(), cc.getY()).addEntity(te);
			chunkManager.getChunk(cc.getX(), cc.getY()).chunkUpdate();
			return true;
		}
	}
	
	public boolean hasTileEntity(int x, int y) {
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		boolean hasTileEntity = chunkManager.getChunk(cc.getX(), cc.getY()).hasTileEntity(x, y);
		return hasTileEntity;
	}
	
	public void removeTileEntity(int x, int y) {	
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		chunkManager.getChunk(cc.getX(), cc.getY()).removeTileEntity(x, y);
	}
	
	public int getItemID(int x, int y) {
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		int itemID = chunkManager.getChunk(cc.getX(), cc.getY()).getItemID(x, y);
		return itemID;
	}
	
	public boolean addEntity(Entity e) {
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(e.getBlockX()), chunkManager.convertBlockToChunkY(e.getBlockY()));
		Chunk c = chunkManager.getChunk(cc.getX(), cc.getY());
		c.addEntity(e);
		return true;
	}
	
	public Entity getEntity(int x, int y) {
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		return chunkManager.getChunk(cc.getX(), cc.getY()).getEntity(x, y);
	}
	
	public boolean isBlocked(int x, int y) {		
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		boolean hasTileEntity = chunkManager.getChunk(cc.getX(), cc.getY()).hasTileEntity(x, y);
		boolean hasMob = chunkManager.getChunk(cc.getX(), cc.getY()).hasMob(x, y);
		x = x - (cc.getX() * CHUNK_SIZE);
		y = y - (cc.getY() * CHUNK_SIZE);
		if (!Block.blocks[chunkManager.getChunk(cc.getX(), cc.getY()).getBlocksArray()[x][y]].isTransparent() || hasTileEntity || hasMob) {
			return true;
		} else {
			return false;
		}
	}
	
	public int numberOfMobs(int x, int y) {
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		return chunkManager.getChunk(cc.getX(), cc.getY()).numberOfMobs(x, y);
	}
	
	public boolean isTouchingBlock(Entity e) {
		int bx = e.getBlockX();
		int by = e.getBlockY();
		Rectangle[] blocks = {
				new Rectangle(bx * BLOCK_SIZE, by * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE),
				new Rectangle((bx + 1) * BLOCK_SIZE, by * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE),
				new Rectangle((bx - 1) * BLOCK_SIZE, by * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE),
				new Rectangle(bx * BLOCK_SIZE, (by + 1) * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE),
				new Rectangle(bx * BLOCK_SIZE, (by - 1) * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE),
				new Rectangle((bx + 1) * BLOCK_SIZE, (by + 1) * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE),
				new Rectangle((bx - 1) * BLOCK_SIZE, (by - 1) * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE),
				new Rectangle((bx + 1) * BLOCK_SIZE, (by + 1) * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE),
				new Rectangle((bx - 1) * BLOCK_SIZE, (by - 1) * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE)
		};
		for (int i = 0; i < blocks.length; i++) {
			if (blocks[i].intersects(e.getBounds())) {
				boolean blocked = isBlocked(World.getBlockXForX((int)blocks[i].getX()), World.getBlockYForY((int)blocks[i].getY()));
				if (blocked) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean addBlock(int id, int x, int y) {
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		boolean hasTileEntity = chunkManager.getChunk(cc.getX(), cc.getY()).hasTileEntity(x, y);
		boolean hasMob = (numberOfMobs(x, y) > 0) ? true : false;
		if (player.getBlockX() == x && player.getBlockY() == y) {
			hasMob = true;
		}
		x = x - (cc.getX() * CHUNK_SIZE);
		y = y - (cc.getY() * CHUNK_SIZE);
		if (chunkManager.getChunk(cc.getX(), cc.getY()).getBlocksArray()[x][y] != 0 || hasTileEntity || hasMob) {
			return false;
		} else {
			chunkManager.getChunk(cc.getX(), cc.getY()).getBlocksArray()[x][y] = id;
			chunkManager.getChunk(cc.getX(), cc.getY()).getDamageArray()[x][y] = Block.blocks[id].getMaxHealth();
			chunkManager.getChunk(cc.getX(), cc.getY()).chunkUpdate();
			if (Block.blocks[id].isLightSource()) {
				transparentUpdate = false;
				updateLight = new Light(cc, x, y);
				lightingUpdate = true;
				add = true;
			} else {
				transparentUpdate = true;
				try {
					updateLight = new Light(cc, x, y);
					lightingUpdate = true;
					add = true;
				} catch (Exception e) {
					// TODO Auto-generated catch block
				}

			}
			return true;
		}
	}
	
	public void addPathing(Mob m, int x, int y) {
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		x = x - (cc.getX() * CHUNK_SIZE);
		y = y - (cc.getY() * CHUNK_SIZE);
		chunkManager.getChunk(cc.getX(), cc.getY()).addPathing(m, x, y);
	}
	
	public void removePathing(int x, int y) {
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		x = x - (cc.getX() * CHUNK_SIZE);
		y = y - (cc.getY() * CHUNK_SIZE);
		chunkManager.getChunk(cc.getX(), cc.getY()).removePathing(x, y);
	}
	
	public boolean damageBlock(int damage, int x, int y) {
		ChunkCoord cc = new ChunkCoord(chunkManager.convertBlockToChunkX(x), chunkManager.convertBlockToChunkY(y));
		int nx = x - (cc.getX() * CHUNK_SIZE);
		int ny = y - (cc.getY() * CHUNK_SIZE);
		if (chunkManager.getChunk(cc.getX(), cc.getY()).getBlocksArray()[nx][ny] != 0) {
			Chunk c = chunkManager.getChunk(cc.getX(), cc.getY());
			c.getDamageArray()[nx][ny] -= damage;
			if (c.getDamageArray()[nx][ny] < 1) {
				int itemID = Block.blocks[c.getBlocksArray()[nx][ny]].getItemID();
				transparentUpdate = !Block.blocks[c.getBlocksArray()[nx][ny]].isLightSource();
				c.getBlocksArray()[nx][ny] = 0;
				lightingUpdate = true;
				add = false;
				updateLight = new Light(cc, nx, ny);
				addTileEntity(new Item(itemID), x, y);
			}
			return true;
		} else {
			return false;
		}
	}
	
	public boolean shouldUpdateLighting() {
		return lightingUpdate;
	}
	
	public void updatedLighting() {
		lightingUpdate = false;
	}
		
	public synchronized void updateLighting() {
		runningLightings++;
		Game.debug("Started lighter. Now " + runningLightings + " are running.");
				Lighter lighter = new Lighter(chunkManager.getChunks(), updateLight.getChunkCoord(), updateLight.getX(), updateLight.getY(), add, transparentUpdate) {
			@Override
			protected void done() {
				try {
					runningLightings--;
					Game.debug("DONE and there are " + runningLightings + " running now.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		
		lighter.execute();
	}
	
	public void render(GameContainer gc, Graphics g) {
		int cameraChunkX = chunkManager.convertToChunkX(cameraX + SCREEN_WIDTH / 2);
		int cameraChunkY = chunkManager.convertToChunkY(cameraY + SCREEN_HEIGHT / 2);
		
		int startx = cameraX / (BLOCK_SIZE * WORLD_SCALE) - 2;
		int endx = (cameraX + SCREEN_WIDTH) / (BLOCK_SIZE * WORLD_SCALE) + 2;
		int starty = cameraY / (BLOCK_SIZE * WORLD_SCALE) - 2;
		int endy = (cameraY + SCREEN_HEIGHT) / (BLOCK_SIZE * WORLD_SCALE) + 2;
		for (int i = startx; i < endx; i++) {
			for (int c = starty; c < endy; c++) {
				g.drawImage(dirtBackground, i * BLOCK_SIZE * WORLD_SCALE - cameraX, c * BLOCK_SIZE * WORLD_SCALE - cameraY);
			}
		}
		
		Set<ChunkCoord> set = chunkManager.getChunks().keySet();
		Iterator<ChunkCoord> itr = set.iterator();
				
		while(itr.hasNext()) {
			ChunkCoord o = itr.next();
			if (Math.abs(cameraChunkX - o.getX()) < 2 && Math.abs(cameraChunkY - o.getY()) < 2) {
				chunkManager.getChunk(o.getX(), o.getY()).render(gc, g, cameraX, cameraY, player);
			}
		}
		
		player.render(g, player.getX() * WORLD_SCALE - getCameraX(), player.getY() * WORLD_SCALE - getCameraY());
		player.renderPings(g, getCameraX(), getCameraY());
		
		itr = set.iterator();
		
		while(itr.hasNext()) {
			ChunkCoord o = itr.next();
			if (Math.abs(cameraChunkX - o.getX()) < 2 && Math.abs(cameraChunkY - o.getY()) < 2) {
				chunkManager.getChunk(o.getX(), o.getY()).renderLighting(gc, g, cameraX, cameraY);
			}
		}
		
		player.renderUI(g);
	}
	
	public void update(GameContainer gc, int delta) {
		if (shouldUpdateLighting()) {
			updateLighting();
			updatedLighting();
		}
		
		int cameraChunkX = chunkManager.convertToChunkX(cameraX + SCREEN_WIDTH / 2);
		int cameraChunkY = chunkManager.convertToChunkY(cameraY + SCREEN_HEIGHT / 2);
		
		Set<ChunkCoord> set = chunkManager.getChunks().keySet();
		Iterator<ChunkCoord> itr = set.iterator();
				
		while(itr.hasNext()) {
			ChunkCoord o = itr.next();
			if (Math.abs(cameraChunkX - o.getX()) < 2 && Math.abs(cameraChunkY - o.getY()) < 2) {
				chunkManager.getChunk(o.getX(), o.getY()).update(gc, this, delta, player);
			}
		}
		chunkManager.update(player, gc, this, delta);
		player.update(gc, this, delta);
		setCameraPosition(player.getX() * WORLD_SCALE - SCREEN_WIDTH / 2, player.getY() * WORLD_SCALE - SCREEN_HEIGHT / 2);
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public void savePlayerToFile() {
		player.saveToFile();
	}
	
	public static int getBlockXForX(int x) {
		int bx = (int) Math.floor((x + 1) / BLOCK_SIZE);
		if (x + 1 < 0) {
			bx--;
		}
		return bx;
	}
	
	public static int getBlockYForY(int y) {
		int by = (int) Math.floor((y + 1) / BLOCK_SIZE);
		if (y + 1 < 0) {
			by--;
		}
		return by;
	}
	
	public int getBlockXForMouseX(int x) {
		x += cameraX;
		int blockX = (int) Math.floor(x / (BLOCK_SIZE * WORLD_SCALE));
		if (x < 0) {
			blockX--;
		}
		return blockX;
	}
	
	public int getBlockYForMouseY(int y) {
		y += cameraY;
		int blockY = (int) Math.floor(y / (BLOCK_SIZE * WORLD_SCALE));
		if (y < 0) {
			blockY--;
		}
		return blockY;
	}
	
	public void getPath(Mob m, int startX, int startY, int endX, int endY, final PathResponse pr) {
		final PositiveWorld pw = new PositiveWorld(chunkManager, m);
		ThreadedPathFinder tpf = new ThreadedPathFinder(pw, null, startX - pw.getBlockOffsetX(), startY - pw.getBlockOffsetY(), endX - pw.getBlockOffsetX(), endY - pw.getBlockOffsetY()) {
			@Override
			protected void done() {
				try {
					if (get() != null) {
						pr.done(new ShiftedPath(get(), pw.getBlockOffsetX(), pw.getBlockOffsetY()));
					} else {
						pr.done(null);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
				
		tpf.execute();
	}
	
	public void getPath(int startX, int startY, int endX, int endY, final PathResponse pr) {
		final PositiveWorld pw = new PositiveWorld(chunkManager);
		ThreadedPathFinder tpf = new ThreadedPathFinder(pw, null, startX - pw.getBlockOffsetX(), startY - pw.getBlockOffsetY(), endX - pw.getBlockOffsetX(), endY - pw.getBlockOffsetY()) {
			@Override
			protected void done() {
				try {
					if (get() != null) {
						pr.done(new ShiftedPath(get(), pw.getBlockOffsetX(), pw.getBlockOffsetY()));
					} else {
						pr.done(null);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
				
		tpf.execute();
	}
	
	public PositiveWorld getPositiveWorld() {
		return new PositiveWorld(chunkManager);
	}
	
	public ChunkManager getChunkManager() {
		return chunkManager;
	}
	
	private int getSign(int number) {
		if (number < 0) {
			return -1;
		} else {
			return 1;
		}
	}
	
	public boolean lineOfSight(int startX, int startY, int endX, int endY) {
		PositiveWorld pw = new PositiveWorld(chunkManager);
		
		startX -= pw.getBlockOffsetX();
		startY -= pw.getBlockOffsetY();
		endX -= pw.getBlockOffsetX();
		endY -= pw.getBlockOffsetY();
		
		if (startX == endX && startY == endY) {
			return true;
		}
				
		int t, x, y, ax, ay, sx, sy, dx, dy;
		
		dx = startX - endX;
		dy = startY - endY;
		
		ax = Math.abs(dx) << 1;
		ay = Math.abs(dy) << 1;
		
		sx = getSign(dx);
		sy = getSign(dy);
		
		x = endX;
		y = endY;
		
		if (ax > ay) {
			t = ay - (ax >> 1);
			do {
				if (t >= 0) {
					y += sy;
					t -= ax;
				}
				x += sx;
				t += ay;
								
				if (x == startX && y == startY) {
					return true;
				}
			} while (!pw.blocked(null, x, y));
			return false;
		} else {
			t = ax - (ay >> 1);
			do {
				if (t >= 0) {
					x += sx;
					t -= ay;
				}
				y += sy;
				t += ax;
				
				if (x == startX && y == startY) {
					return true;
				}
			} while (!pw.blocked(null, x, y));
			return false;
		}
	}

	public int getCameraX() {
		return cameraX;
	}
	
	public int getCameraY() {
		return cameraY;
	}
	
	public void setCameraPosition(int x, int y) {
		this.cameraX = x;
		this.cameraY = y;
	}
	
	public void saveWorld() {
		chunkManager.saveChunks();
	}
	
	public float distanceBetween(Entity e1, Entity e2) {
		float dist = (float) Math.sqrt(Math.pow(e1.getX() - e2.getX(), 2) + Math.pow(e1.getY() - e2.getY(), 2));
		dist /= BLOCK_SIZE;
		return dist;
	}

	public int getXForMouseX(int mouseX) {
		// TODO Auto-generated method stub
		return (mouseX + cameraX) / WORLD_SCALE;
	}
	
	public int getYForMouseY(int mouseY) {
		// TODO Auto-generated method stub
		return (mouseY + cameraY) / WORLD_SCALE;
	}
	
	public Mob getClosestMob(int x, int y) {
		ChunkCoord cc1 = new ChunkCoord(chunkManager.convertBlockToChunkX(getBlockXForX(x)), chunkManager.convertBlockToChunkY(getBlockYForY(y)));
		ChunkCoord cc2 = new ChunkCoord(chunkManager.convertBlockToChunkX(getBlockXForX(x)) - 1, chunkManager.convertBlockToChunkY(getBlockYForY(y)));
		ChunkCoord cc3 = new ChunkCoord(chunkManager.convertBlockToChunkX(getBlockXForX(x)) - 1, chunkManager.convertBlockToChunkY(getBlockYForY(y)) - 1);
		ChunkCoord cc4 = new ChunkCoord(chunkManager.convertBlockToChunkX(getBlockXForX(x)), chunkManager.convertBlockToChunkY(getBlockYForY(y)) - 1);
		ChunkCoord cc5 = new ChunkCoord(chunkManager.convertBlockToChunkX(getBlockXForX(x)) + 1, chunkManager.convertBlockToChunkY(getBlockYForY(y)) - 1);
		ChunkCoord cc6 = new ChunkCoord(chunkManager.convertBlockToChunkX(getBlockXForX(x)) + 1, chunkManager.convertBlockToChunkY(getBlockYForY(y)));
		ChunkCoord cc7 = new ChunkCoord(chunkManager.convertBlockToChunkX(getBlockXForX(x)) + 1, chunkManager.convertBlockToChunkY(getBlockYForY(y)) + 1);
		ChunkCoord cc8 = new ChunkCoord(chunkManager.convertBlockToChunkX(getBlockXForX(x)), chunkManager.convertBlockToChunkY(getBlockYForY(y)) + 1);
		ChunkCoord cc9 = new ChunkCoord(chunkManager.convertBlockToChunkX(getBlockXForX(x)) - 1, chunkManager.convertBlockToChunkY(getBlockYForY(y)) + 1);
		ArrayList<Entity> entities = new ArrayList<Entity>();
		try {
			entities.addAll(chunkManager.getChunk(cc1.getX(), cc1.getY()).getEntities());
			entities.addAll(chunkManager.getChunk(cc2.getX(), cc2.getY()).getEntities());
			entities.addAll(chunkManager.getChunk(cc3.getX(), cc3.getY()).getEntities());
			entities.addAll(chunkManager.getChunk(cc4.getX(), cc4.getY()).getEntities());
			entities.addAll(chunkManager.getChunk(cc5.getX(), cc5.getY()).getEntities());
			entities.addAll(chunkManager.getChunk(cc6.getX(), cc6.getY()).getEntities());
			entities.addAll(chunkManager.getChunk(cc7.getX(), cc7.getY()).getEntities());
			entities.addAll(chunkManager.getChunk(cc8.getX(), cc8.getY()).getEntities());
			entities.addAll(chunkManager.getChunk(cc9.getX(), cc9.getY()).getEntities());
		} catch (Exception e) {
			//e.printStackTrace();
		}
		Entity lowestMob = null;
		float lowestDistance = 0;
		for (int i = 0; i < entities.size(); i++) {
			float dist = (float) Math.sqrt(Math.pow(x - (entities.get(i).getX() + (BLOCK_SIZE / 2)), 2) + Math.pow(y - (entities.get(i).getY() + (BLOCK_SIZE / 2)), 2));
			if ((i == 0 || dist < lowestDistance) && entities.get(i) instanceof Mob) {
				lowestDistance = dist;
				lowestMob = entities.get(i);
			}
		}
		return (Mob)lowestMob;
	}
			
}
