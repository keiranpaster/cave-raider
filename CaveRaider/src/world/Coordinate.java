package world;

import com.keirp.caveraider.Commons;

public class Coordinate implements Commons {
	
	private ChunkCoord cc;
	private int x, y;
	private double rx, ry;

	public Coordinate(ChunkCoord cc, double x, double y) {
		this.cc = cc;
		this.x = (int)x;
		this.y = (int)y;
		this.rx = cc.getX() * CHUNK_SIZE + x;
		this.ry = cc.getY() * CHUNK_SIZE + y;
	}
	
	public Coordinate(double x, double y) {
		int ix, iy, cx, cy;
		if (x > 0) {
			ix = (int) Math.floor(x);
			cx = (int) Math.floor(ix / CHUNK_SIZE);
		} else {
			ix = (int) Math.ceil(x);
			cx = (int) Math.ceil(ix / CHUNK_SIZE);
		}
		if (y > 0) {
			iy = (int) Math.floor(x);
			cy = (int) Math.floor(iy / CHUNK_SIZE);
		} else {
			iy = (int) Math.ceil(x);
			cy = (int) Math.ceil(iy / CHUNK_SIZE);
		}
		ChunkCoord cc = new ChunkCoord(cx, cy);
		this.x = ix - (cc.getX() * CHUNK_SIZE);
		this.y = iy - (cc.getY() * CHUNK_SIZE);
		this.rx = x;
		this.ry = y;
		this.cc = cc;
	}
	
	public Coordinate(String s) {
		String ps[] = s.split(",");
		this.cc = new ChunkCoord(Integer.parseInt(ps[0]), Integer.parseInt(ps[1]));
		this.x = Integer.parseInt(ps[2]);
		this.y = Integer.parseInt(ps[3]);
		this.rx = Double.parseDouble(ps[4]);
		this.ry = Double.parseDouble(ps[5]);
	}
	
	public String toString() {
		return cc.getX() + "," + cc.getY() + "," + x + "," + y + "," + rx + "," + ry;
	}
	
	public ChunkCoord getChunkCoord() {
		return cc;
	}
	
	public int getInnerX() {
		return x;
	}
	
	public int getInnerY() {
		return y;
	}
	
	public double getX() {
		return rx;
	}
	
	public double getY() {
		return ry;
	}
	
	public int getPixelX() {
		return (int) rx * BLOCK_SIZE * WORLD_SCALE;
	}
	
	public int getPixelY() {
		return (int) ry * BLOCK_SIZE * WORLD_SCALE;
	}
	
}
