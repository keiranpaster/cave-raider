package Art;

import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;

public class SoundManager {
	
	Music[] songs = new Music[256];
	
	public SoundManager() {
		try {
			songs[0] = new Music("sounds/music/main_loop.wav");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void playSongWithID(int id, float pitch, float volume) {
		songs[id].play(pitch, volume);
	}
	
	public void playSongWithID(int id) {
		songs[id].play();
	}
	
}
