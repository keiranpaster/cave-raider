package Art;

import org.newdawn.slick.Image;

import com.keirp.caveraider.Commons;

public class GameImages implements Commons {
	
	public static Image[][] images = ImageLoader.chop("images/spriteSheet.png", WORLD_SCALE, 16, 16);
	public static Image[][] inventoryImages = ImageLoader.chop("images/spriteSheet.png", INVENTORY_ITEM_SCALE, 16, 16);
	public static Image[][] itemImages = ImageLoader.chop("images/spriteSheet.png", ITEM_SCALE, 16, 16);
	public static Image[][] handImages = ImageLoader.chop("images/spriteSheet.png", PLAYER_HAND_SCALE, 16, 16);
	public static Image[][] pingAnimation = ImageLoader.chop("ui/pingEffect.png", 2, 16, 16);

}
