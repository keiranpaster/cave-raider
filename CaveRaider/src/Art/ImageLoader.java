package Art;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import com.keirp.caveraider.Commons;

public class ImageLoader implements Commons {
	
	public static Image getImage(String imagePath) throws SlickException {
		try {
			return new Image(imagePath);
		} catch (SlickException e) {
			throw e;
		}
	}
	
	public static Image get(String imagePath, float scale) {
		try {
			Image result = new Image(imagePath);
			result.setFilter(Image.FILTER_NEAREST);
			result = result.getScaledCopy(scale);
			return result;
		} catch (SlickException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Image getScaledImage(Image result, float scale) {
		result.setFilter(Image.FILTER_NEAREST);
		result = result.getScaledCopy(scale);
		return result;
	}
	
	public static Image[][] chop(String img, float scale, int w, int h) {
//		w *= scale;
//		h *= scale;
		Image bigPicture = get(img, 1);
		
		int xt = bigPicture.getWidth() / w;
		int yt = bigPicture.getHeight() / h;
		
		Image[][] result = new Image[xt][yt];
		for (int i = 0; i < xt; i++) {
			for (int c = 0; c < yt; c++) {
				result[i][c] = getScaledImage(bigPicture.getSubImage(i * w, c * h, w, h), scale);
			}
		}
		return result;
	}
	
}