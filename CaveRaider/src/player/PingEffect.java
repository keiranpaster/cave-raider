package player;

import org.newdawn.slick.Graphics;

import Art.GameImages;

import com.keirp.caveraider.Commons;

public class PingEffect implements Commons {
	
	long startTime;
	int stage, x, y;
	int FRAME_LENGTH = 30;
	boolean done = false;

	public PingEffect(int x, int y) {
		this.startTime = System.currentTimeMillis();
		this.x = x;
		this.y = y;
		stage = 0;
	}
	
	public boolean donePinging() {
		return done;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void update() {
		int deltaTime = (int) (System.currentTimeMillis() - startTime);
		if (deltaTime < FRAME_LENGTH) {
			stage = 0;
		} else if (deltaTime < FRAME_LENGTH * 2) {
			stage = 1;
		} else if (deltaTime < FRAME_LENGTH * 3) {
			stage = 2;
		} else if (deltaTime < FRAME_LENGTH * 4) {
			stage = 3;
		} else if (deltaTime < FRAME_LENGTH * 5) {
			stage = 4;
		} else if (deltaTime < FRAME_LENGTH * 6) {
			done = true;
		}
	}
	
	public void render(Graphics g, int x, int y) {
//		g.drawImage(GameImages.pingAnimation[stage][0], x, y);
	}
	
}
