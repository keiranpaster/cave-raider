package player;

import java.util.ArrayList;

import inventory.Inventory;
import inventory.InventoryItem;
import items.Item;

import org.newdawn.slick.*;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;

import world.MovementIntent;
import world.PathResponse;
import world.ShiftedPath;
import world.World;

import Art.GameImages;
import com.keirp.caveraider.Game;
import com.keirp.caveraider.Play;

import com.keirp.caveraider.Commons;

import entities.Bat;
import entities.Entity;
import entities.Mob;

public class Player extends Mob implements Commons {
	
	int walkingState = 0;
	long animationTime = 0;
	int animationInterval = 100;
	int walkingLeg = 0;
	long swingTime = 0;
	private Mob targetMob;
	private Mob hoverMob;
	
	private final static int player = 0;
	private Image[][] images = {
			{
				GameImages.images[0][3],
				GameImages.images[0][4],
				GameImages.images[0][5],
			},
			{
				GameImages.images[1][3],
				GameImages.images[1][4],
				GameImages.images[1][5],
			},
			{
				GameImages.images[2][3],
				GameImages.images[2][4],
				GameImages.images[2][5],
			},
			{
				GameImages.images[3][3],
				GameImages.images[3][4],
				GameImages.images[3][5],
			}
	};
	
	private int[][] handPos = {
			{
				12,
				10,
				1
			},
			{
				3,
				10,
				-1
			},
			{
				4,
				9,
				-1
			},
			{
				11,
				9,
				1
			}
	};
	
	private Inventory inventory;
	
	private ArrayList<PingEffect> pings = new ArrayList<PingEffect>();
		
	public Player() {
		super(player, 0, 0);
		this.inventory = new Inventory();
		this.setMovementSpeed(.08f);
		
		
		for (int i = 0; i < 64; i++) {		
			inventory.pushItem(new InventoryItem(1));
		}
		
		inventory.pushItem(new InventoryItem(4));
		inventory.pushItem(new InventoryItem(9));
		inventory.pushItem(new InventoryItem(5));
		inventory.pushItem(new InventoryItem(3));
		
		for (int i = 0; i < 64; i++) {
			inventory.pushItem(new InventoryItem(14));
		}
				
		for (int i = 0; i < 64; i++) {		
			inventory.pushItem(new InventoryItem(2));
		}
	}
	
	public void saveToFile() {
		
	}
	
	private class PlayerFile {
		
		public PlayerFile(Inventory)
		
	}
	
	@Override
	public void update(GameContainer gc, World world, int delta) {
		
		inventory.update(gc, world, this);
		
		if (!pings.isEmpty()) {
			for (int i = 0; i < pings.size(); i++) {
				pings.get(i).update();
				if (pings.get(i).donePinging()) {
					pings.remove(i);
					i--;
				}
			}
		}
		
		Input input = gc.getInput();
		final Input i = gc.getInput();
		
		if (input.isKeyPressed(Input.KEY_B)) {
			world.addEntity(new Bat(0, 0, 0));
		}
		
		if (input.isKeyDown(Input.KEY_UP) || input.isKeyDown(Input.KEY_W)) {
			changeY((int) (-PLAYER_SPEED * delta));
		}
		if (input.isKeyDown(Input.KEY_DOWN) || input.isKeyDown(Input.KEY_S)) {
			changeY((int) (PLAYER_SPEED * delta));
		}
		if (input.isKeyDown(Input.KEY_LEFT) || input.isKeyDown(Input.KEY_A)) {
			changeX((int) (-PLAYER_SPEED * delta));
		}
		if (input.isKeyDown(Input.KEY_RIGHT) || input.isKeyDown(Input.KEY_D)) {
			changeX((int) (PLAYER_SPEED * delta));
		}
				
		if (!inventory.isFullGridOpen() && input.isMousePressed(0)) {
			final InventoryItem selectedItem = inventory.getSelectedItem();
			final World w = world;
			final MovementIntent intent;
			
			try {				
				if (w.getItemID(world.getBlockXForMouseX(input.getMouseX()), world.getBlockYForMouseY(input.getMouseY())) != 0) {
					intent = new MovementIntent(world.getBlockXForMouseX(input.getMouseX()), world.getBlockYForMouseY(input.getMouseY()), 1) {
						@Override
						public void movementFinished() {
							inventory.pushItem(new InventoryItem(w.getItemID(this.getX(), this.getY())));
							w.removeTileEntity(this.getX(), this.getY());
						}
					};
				} else {
					intent = new MovementIntent(world.getBlockXForMouseX(input.getMouseX()), world.getBlockYForMouseY(input.getMouseY()), selectedItem.getMinRange()) {
						@Override
						public void movementFinished() {
							useItem(selectedItem, this.getX(), this.getY(), w);
						}
					};
				}
				
				if (intent.shouldStopMoving((int)Math.floor(this.getX() / BLOCK_SIZE * WORLD_SCALE), (int)Math.floor(this.getY() / BLOCK_SIZE * WORLD_SCALE), world)) {
					intent.movementFinished();
				} else {
					world.getPath(world.getBlockXForX(getX()), world.getBlockYForY(getY()), world.getBlockXForMouseX(input.getMouseX()), world.getBlockYForMouseY(input.getMouseY()),  new PathResponse() {
						@Override
						public void done(ShiftedPath path) {
							if (path != null && intent != null) {
								pathToQueueWithIntent(path, intent, w);
							}
						}
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
				Game.debug("Cannot move to that square.");
			}
		}
		
		if (!inventory.isFullGridOpen() && input.isMousePressed(1)) {
			final World w = world;
			try {
				Game.debug(world.getBlockXForMouseX(input.getMouseX()) + "," + world.getBlockYForMouseY(input.getMouseY()));

				pings.add(new PingEffect(world.getBlockXForMouseX(input.getMouseX()), world.getBlockYForMouseY(input.getMouseY())));
				world.getPath(world.getBlockXForX(getX()), world.getBlockYForY(getY()), world.getBlockXForMouseX(input.getMouseX()), world.getBlockYForMouseY(input.getMouseY()),  new PathResponse() {
					@Override
					public void done(ShiftedPath path) {
						if (path != null) {
							pathToQueue(path, w);
						}
					}
				});
			} catch (Exception e) {
				Game.debug("Cannot move to that square.");
			}
		}
		
		if ((this.hasAnimate() || walkingState != 0) && System.currentTimeMillis() >= animationTime) {
			animationTime = System.currentTimeMillis() + animationInterval;
			switch (walkingState) {
			case 0:
				if (walkingLeg == 0) {
					walkingState = 1;
				} else {
					walkingState = 2;
				}
				break;
			case 1:
				walkingState = 0;
				walkingLeg = 1;
				break;
			case 2:
				walkingState = 0;
				walkingLeg = 0;
				break;
			}
		} else if (!this.hasAnimate()) {
			walkingState = 0;
			faceTowards(world.getBlockXForMouseX(input.getMouseX()), world.getBlockYForMouseY(input.getMouseY()));
		}
		calculateHoverMob(input, world);
		this.animateMovements(delta, world);
	}
	
	public void swingItem() {
		swingTime = System.currentTimeMillis() + PLAYER_ITEM_SWING_TIME;
	}
	
	public int getItemRotation() { 
		int swingDiff = (int) (swingTime - System.currentTimeMillis());
		if (swingDiff > 0 && swingTime != 0) {
			swingDiff -= PLAYER_ITEM_SWING_TIME;
			return (int) ((-PLAYER_ITEM_SWING_DISTANCE/Math.pow(PLAYER_ITEM_SWING_TIME, 2)) * Math.pow(swingDiff, 2) + PLAYER_ITEM_SWING_DISTANCE);
		} else {
			return 0;
		}
	}
	
	public void useItem(InventoryItem item, int x, int y, World world) {
		faceTowards(x, y);
		inventory.useItem(item, x, y, this, world);
		swingItem();
	}
	
	public void faceTowards(int x, int y) {
		int dx = this.getBlockX() - x;
		int dy = this.getBlockY() - y;
		if (Math.abs(dx) > Math.abs(dy)) {
			if (dx > 0) {
				this.setDirection(2);
			} else {
				this.setDirection(3);
			}
		} else {
			if (dy < 0) {
				this.setDirection(0);
			} else {
				this.setDirection(1);
			}
		}
	}
	
	public void renderPings(Graphics g, int cameraX, int cameraY) {
		if (!pings.isEmpty()) {
			for (int i = 0; i < pings.size(); i++) {
				pings.get(i).render(g, pings.get(i).getX() * BLOCK_SIZE * WORLD_SCALE - cameraX, pings.get(i).getY() * BLOCK_SIZE * WORLD_SCALE - cameraY);
			}
		}
	}
 
	@Override
	public void render(Graphics g, int x, int y) {
		g.drawImage(images[this.getDirection()][walkingState], x, y);
		Image item = GameImages.handImages[inventory.getSelectedItem().getImageX()][inventory.getSelectedItem().getImageY()];
		item.setCenterOfRotation(Math.abs(BLOCK_SIZE * ((handPos[this.getDirection()][2] == -1) ? 1 : 0) - Item.items[inventory.getSelectedItem().getItemID()].getAttributeAsIntegerList("center")[0] * PLAYER_HAND_SCALE), Item.items[inventory.getSelectedItem().getItemID()].getAttributeAsIntegerList("center")[1] * PLAYER_HAND_SCALE);
		item = item.getFlippedCopy((handPos[this.getDirection()][2] == -1) ? true : false, false);
		item.setRotation(getItemRotation() * handPos[this.getDirection()][2]);
		g.drawImage(item, x + (handPos[this.getDirection()][0]) * WORLD_SCALE - (Math.abs(BLOCK_SIZE * WORLD_SCALE / WORLD_SCALE * ((handPos[this.getDirection()][2] == -1) ? 1 : 0) - Item.items[inventory.getSelectedItem().getItemID()].getAttributeAsIntegerList("center")[0])) * PLAYER_HAND_SCALE, y + (handPos[this.getDirection()][1]) * WORLD_SCALE - Item.items[inventory.getSelectedItem().getItemID()].getAttributeAsIntegerList("center")[1] * PLAYER_HAND_SCALE);
	}
	
	public void renderUI(Graphics g) {
		inventory.render(g);
	}

	@Override
	public ArrayList<Attr> getSaveAttributes(Document doc) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void calculateHoverMob(Input i, World w) {
		int mx = w.getXForMouseX(i.getMouseX());
		int my = w.getYForMouseY(i.getMouseY());
		Mob hoverMobTemp = w.getClosestMob(mx, my);
		if (hoverMobTemp != null) {
			double dist = Math.sqrt(Math.pow(mx - hoverMobTemp.getX(), 2) + Math.pow(my - hoverMobTemp.getY(), 2));
			if (dist < BLOCK_SIZE / 2 * WORLD_SCALE) {
				hoverMob = hoverMobTemp;
			}
			if (i.isMouseButtonDown(0)) {
				lockTargetMob();
			}
		}
	}
	
	public void lockTargetMob() {
		targetMob = hoverMob;
	}
	
	public Mob getHoverMob() {
		return hoverMob;
	}
	
	public Mob getTargetMob() {
		return targetMob;
	}

}
