package player;

import com.keirp.caveraider.Commons;

public class Camera implements Commons {

	private double x;
	private double y;
	
	public Camera() {
		x = 0;
		y = 0;
	}
	
	public int getX() {
		return (int) x;
	}
	
	public int getY() {
		return (int) y;
	}
	
	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
}
